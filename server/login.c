#include "protocol.h"

int login(void *arg)
{
	int i,ret;
	int sockfd = *((int *)arg);	
	char msgbuf[MSGBUF];
	
	char buf[64];
	char type;
	char log_buf[64] = {0};
	FILE *fp;
	FILE *fp1;
	FILE *fp2;
	char wifiap[26];
	char date[12];
	time_t rawtime;
	struct tm *info;
		
	while(1)
	{
		printf("sockfd = %d\n",sockfd);
		printf("**********login***************\n");
		//验证信息读取
		if((fp = fopen("/home/root/wifi/rtl_8192.conf","rb")) == NULL)
		{
			printf("can not open wifi config file\n");
			return 0;
		}
		memset(buf,0,64);
		fgets(buf, 64, fp);
		printf("%s", buf);
		memset(buf,0,64);
		fgets(buf, 64, fp);
		printf("%s", buf);
		memset(wifiap,0,26);
		for(i = 0; i < 18; i++)
		{
			wifiap[i] = buf[5+i];
		}
		printf("%s\n", wifiap);
		
		time( &rawtime );
		info = localtime( &rawtime );
		memset(date,0,12);
		strftime(date, sizeof(date), "%Y %m %d", info);
		printf("%s\n", date);
		
		strcat(wifiap, date);
		printf("%s\n", wifiap);
		
		wifiap[22] = wifiap[23];
		wifiap[23] = wifiap[24];
		wifiap[24] = wifiap[26];
		wifiap[25] = wifiap[27];
		wifiap[26] = 0;
		wifiap[27] = 0;
		printf("%s\n", wifiap);
		
		if((fp1 = fopen("/home/root/server/source.txt","wb")) == NULL)
		{
			printf("can not open source.txt\n");
			return 0;
		}
		fwrite( wifiap, 1, sizeof(wifiap)+2, fp1);
		fclose(fp1);
		
		system("/home/root/server/run_des -e keyfile source.txt dest.txt");
		
		memset(wifiap,0,26);
		if((fp2 = fopen("/home/root/server/dest.txt","rb")) == NULL)
		{
			printf("can not open dest.txt\n");
			return 0;
		}
		fread( wifiap, 1, sizeof(wifiap), fp2);
		
		fclose(fp2);
		
		struct timeval timeout = {3,0}; 
		//设置接收超时
		setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(struct timeval));
	
		struct timeval timeout_send = {3,0}; 
		//设置发送超时
		setsockopt(sockfd, SOL_SOCKET,SO_SNDTIMEO, (char *)&timeout_send, sizeof(struct timeval));
		
		memset( log_buf, 0, 64);
		if((ret = recv( sockfd, &log_buf, 64, 0)) <= 0)
		{
			printf("recv error\n");
			return 0;
		}

		printf("wifiap=%s\n", wifiap);
		printf("log_buf=%s\n", log_buf);
		ret = 0;
		for(i = 0; i < 24; i++)
			if(log_buf[i] > wifiap[i]) ret = 1;
			else if(log_buf[i] < wifiap[i]) ret = -1;
		printf("ret = %d\n", ret);
		
		if(ret != 0)
		{
			memset( msgbuf, 0, MSGBUF);
			strcpy( msgbuf, "login success!");
			send(sockfd, msgbuf, MSGBUF, 0 );
			fclose(fp);
			return 1;
		}
		else{
			memset( msgbuf, 0, MSGBUF);
			strcpy( msgbuf, "login fail!");
			send(sockfd, msgbuf, MSGBUF, 0 );
			printf("login fail,try again\n");
		}
	}

	fclose(fp);

	return 0;
}
