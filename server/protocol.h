/****************************************************************
	-> file name : protocol.h
	-> author : fluency
	-> Created time : 2017-08-01 15:05
	-> function : all header file and function declaration
****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h> 
#include <pthread.h> 
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>

#include <stdint.h>
#include <getopt.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#include <math.h>
#include <time.h>
#include <limits.h>
#include <stdarg.h>
#include <sys/types.h>    
#include <termios.h>  

#include<asm-generic/ioctl.h>

#include <linux/i2c.h>
#include <linux/i2c-dev.h> 
#define I2C_DEV "/dev/i2c-0"
#define I2C_RDWR 0x0707


#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))
#define debug_pos() printf("%s-%d\n", __FUNCTION__, __LINE__);
#define PI 3.1415926535897932384626433832795028841971
#define RX_BUF_SIZE_DIV4 1024
#define RX_BUF_SIZE_DIV2 (RX_BUF_SIZE_DIV4 *2)
#define RX_BUF_SIZE_DIV8 512

#define PORT 32000  //网络端口
#define SIZEBUF 256 //接收信息buf的大小
#define DATALEN 8  //截取字段
#define MSGBUF 24  //发送信息buf大小
#define MSGSIZE 1024*11  //发送显示信息msg大小

#define DEBUG 0
#define RegNum 44
#define UART_DEVICE "/dev/ttyO1" //uart设备文件名 
#define UART_DEVICE4 "/dev/ttyO4" //uart4设备文件名  
#define DEVICE          "/dev/GPIO_D"
#define MAGIC_NUM       0xDB
#define MAGIC_SET_LOW   _IO(MAGIC_NUM, 0)
#define MAGIC_SET_HIGH  _IO(MAGIC_NUM, 1)
#define MAGIC_GET_GPIO  _IO(MAGIC_NUM, 2)
#define  DELAY (1)
#define GPIO0_23 (0*32 + 23) //P102: [GPMC_AD9/GPIO0_23]
#define GPIO0_26 (0*32 + 26) //P103: [GPMC_AD10/GPIO0_26]
#define GPIO0_27 (0*32 + 27) //P104: [GPMC_AD11/GPIO0_27]
#define GPIO1_14 (1*32 + 14) //P105: [GPMC_AD14/GPIO1_14]
#define GPIO1_15 (1*32 + 15) //P106: [GPMC_AD15/GPIO1_15]

#define GPIO2_6  (2*32 + 6)  //P70：LCD_DATA0/GPIO2_6  2*32+6=70 sync
#define GPIO2_7  (2*32 + 7)  //P71：LCD_DATA1/GPIO2_7  2*32+7=71 clk
#define GPIO2_8  (2*32 + 8)  //P72：LCD_DATA2/GPIO2_8  2*32+8=72 data

int net_init();
int login(void *arg);
int config_setting();
void pthread_spi(void *arg);
void pthread_read(void *arg);

pthread_mutex_t mutex;

int fdFpga;
int flag;   //error 标志
int flag1;  //状态信息
int flag2;  //网卡信息
int flag3;  //频谱
int flag4;  //星座图
int static_socket;

int da_data;
int ad_data;
int wb_data;
int fpga_data;

char pin;//stm32 高低栈管脚状态
char lock;//stm32锁管脚状态
//char buf[SIZEBUF]; //接收信息buf 1+8N模式解析信息


struct Reg{
	char name[32];
	int addr_base;
	int addr_offset;
	int width;
	uint32_t mask;
}tmp;

	