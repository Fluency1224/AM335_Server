#include "protocol.h"

#define ARMVERSION 18062701

struct Status_msg{
	char type[8];
	char ip[15];
	char mac[17];
	char uptime[16];
	char date[20];
	char name[64];
	float longitude;
	float atitude;
	float TLevel;
	float rTHR;
	float sTHR;
	uint32_t version;
	uint32_t bert_uncoded;
	uint32_t bert_coded;
	uint32_t mse_avg;
	uint32_t Code_rate;
	uint32_t Disto;  //Disto_AMAM Disto_AMAP
	uint16_t NBWidth;
	uint8_t QAM;
	uint8_t Ilv_depth;
	uint8_t one_bit; // Digital_Loopback Data_source stack_p
	uint8_t DA_data; //0-128
	uint8_t WB_data; // AD_data 0-7 WB_data 1-8
	uint8_t FPGA_data;	
	uint16_t t;
	uint16_t rh;
}statu_msg;

struct Cons_msg{
	char type[8];
	uint32_t buf[1024];
	char ip[15];
	char mac[17];
	char uptime[16];
	char date[20];
	char name[64];
	float longitude;
	float atitude;
	float TLevel;
	float rTHR;
	float sTHR;
	uint32_t version;
	uint32_t bert_uncoded;
	uint32_t bert_coded;
	uint32_t mse_avg;
	uint32_t Code_rate;
	uint32_t Disto;  //Disto_AMAM Disto_AMAP
	uint16_t NBWidth;
	uint8_t QAM;
	uint8_t Ilv_depth;
	uint8_t one_bit; // Digital_Loopback Data_source stack_p
	uint8_t DA_data; //0-128
	uint8_t WB_data; // AD_data 0-7 WB_data 1-8	
	uint8_t FPGA_data;
	uint16_t t;
	uint16_t rh;
}buf_msg;

struct Net_msg{
	char type[8];
	uint32_t ethn_version1;  //2
	uint32_t ethn_version2;
	uint32_t rx_buffer_size; 
	uint32_t tx_buffer_size; 
	uint32_t local_loopback_enable;
	uint32_t loop_data_enable_override;
	uint32_t xon_threshold;
	uint32_t xoff_threshold;
	uint32_t xoff_rerequeset_timer;
	uint32_t pause_quanta;
	uint32_t rx_packets_detected;
	uint32_t rx_packets_dropped;
	uint32_t tx_packets_detected;
	uint32_t tx_packets_dropped;
	uint32_t mac_version1; //2
	uint32_t mac_version2;  
	uint32_t jumbo_frame_enable1; //2
	uint32_t jumbo_frame_enable2;
	uint32_t vlan_enbale1; //2
	uint32_t vlan_enbale2;
	uint32_t rx_flow_control_enable;
	uint32_t tx_flow_control_enable;
	uint32_t max_frame_enable1; //2
	uint32_t max_frame_enable2;
	uint32_t max_frame_size1; //2
	uint32_t max_frame_size2;
	uint32_t rx_bytes;
	uint32_t rx_good_frames;
	uint32_t rx_crc_errors;
	uint32_t rx_length_errors;
	uint32_t rx_pause_frames;
	uint32_t tx_bytes;
	uint32_t tx_good_frames;
	uint32_t tx_urun_errors;
	uint32_t tx_pause_frames;
   
}net_msg; 

struct Error_msg{
	char type[8];
	int time;
	char number;
	
}error_msg; 

struct Reg reg_r[] = 
{	
	{ "evb_ver_minor",                           0x0,   0,        32,   0xffffff00},
	{ "evb_ver_major",                           0x0,   8,        32,   0xffff00ff},
	{ "evb_config_usb",                          0x0,   16,       32,   0xfffeffff},
	{ "evb_config_serial",                       0x0,   17,	      32,	0xfffdffff},
	{ "evb_config_ethernet",                     0x0,   18,       32,	0xfffbffff},
	{ "evb_config_tdm",	                         0x0,   19,	      32,	0xfff7ffff},
	{ "evb_config_aconf",	                     0x0,  	20,	      32,	0xffefffff},
	{ "evb_config_ethernet_que",                 0x0,  	21,	      32,	0xffdfffff},
	{ "evb_config_spim2",                        0x0,   22,	      32,	0xffbfffff},
	{ "evb_config_usb_ft232",                    0x0,   23,	      32,	0xff7fffff},
	{ "evb_config_spis",                         0x0,   24,	      32,	0xfeffffff},
	{ "evb_config_modem1",                       0x0,   25,	      32,	0xfdffffff},
	{ "evb_config_dpll",                         0x0,   26,	      32,	0xfbffffff},
	{ "evb_config_modem2",                       0x0,   27,	      32,	0xf7ffffff},
	{ "evb_config_ssm",                          0x0,   28,       32,	0xefffffff},
	{ "evb_config_odu",                          0x0,   29,	      32,	0xdfffffff},
	{ "evb_config_xpic_test_mod",                0x0,  	30,	      32,	0xbfffffff},
	{ "evb_config_cpuss",                        0x0,   31,	      32,	0x7fffffff},
	{ "evb_compile_time",                        0x4,   0,	      32,	0x0L      },
	{ "evb_aconf_start",                         0x8,   30,	      32,	0xbfffffff},
	{ "evb_aconf_done",                          0x8,   31,	      32,	0x7fffffff},
	{ "evb_scope_sel",                           0xc,   0,	      32,   0xffffffe0},
	{ "evb_scope_sel_spi",                       0xc,   8,	      32,   0xffffe0ff},
	{ "evb_scope_sel_cpu",                       0xc,   16,	      32,   0xffe0ffff},
	{ "evb_common_scope_sel_ena",                0xc,   31,	      32,   0x7fffffff},
	{ "evb_dac_sync",                            0x10,  0,        32,   0xfffffffe},
	{ "evb_idelay0",                             0x10,  8,	      32,	0xffff00ff},
	{ "evb_idelay1",                           	 0x10,  16,	      32,	0xff00ffff},
	{ "evb_gpo",                                 0x14,  0,	      32,	0xffffff00},
	{ "evb_gpo_oe",                              0x14,  8,	      32,	0xffff00ff},
	{ "evb_gpi",                                 0x14,  16,	      32,	0xff00ffff},
	{ "evb_gpi_irq_ena",                         0x14,  24,	      32,	0xffffff  },
	{ "evb_sw_reset",                            0x18,  0,	      32,	0xfffffffe},
	{ "evb_dlb_ena",                             0x1c,  0,	      32,	0xfffffffe},
	{ "evb_xpi12_swap",                          0x1c,  1,	      32,	0xfffffffd},
	{ "evb_xpi21_swap",                          0x1c,  2,	      32,	0xfffffffb},
	{ "evb_xpic_fifo_sclr",                      0x1c,  4,	      32,	0xffffffef},
	{ "evb_xpi12_dly",                           0x1c,  8,	      32,	0xffffc0ff},
	{ "evb_xpi21_dly",                           0x1c,  16,	      32,	0xffc0ffff},
	{ "evb_xpi12_lvl",                           0x20,  0,	      32,	0xfffff000},
	{ "evb_xpi21_lvl",                           0x20,  16,	      32,	0xf000ffff},
	{ "evb_irq_uc_rx_avail",                     0x28,  0,	      32,	0xfffffffe},
	{ "evb_irq_uc_tx_empty",                     0x28,  1,	      32,	0xfffffffd},
	{ "evb_irq_atpc_local",                      0x28,  2,	      32,	0xfffffffb},
	{ "evb_irq_atpc_remote",                     0x28,  3,	      32,   0xfffffff7},
	{ "evb_irq_dpll_status_change",            	 0x28, 	4,	      32,   0xffffffef},
	{ "evb_irq_dpll_ref_change",                 0x28,  5,	      32,   0xffffffdf},
	{ "evb_irq_gpi_status",                      0x28,  6,	      32,   0xffffffbf},
	{ "evb_irq_tdm_status",                      0x28,  7,	      32,   0xffffff7f},
	{ "evb_irq_uc2_rx_avail",                    0x28,  8,	      32,   0xfffffeff},
	{ "evb_irq_uc2_tx_empty",                    0x28, 	9,	      32,	0xfffffdff},
	{ "evb_irq_atpc2_local",                     0x28,  10,	      32,	0xfffffbff},
	{ "evb_irq_atpc2_remote",                    0x28,  11,	      32,	0xfffff7ff},
	{ "evb_irq_uc_rx_avail_ena",                 0x28,  16,	      32,	0xfffeffff},
	{ "evb_irq_uc_tx_empty_ena",                 0x28,  17,	      32,	0xfffdffff},
	{ "evb_irq_atpc_local_ena",                  0x28,  18,	      32,	0xfffbffff},
	{ "evb_irq_atpc_remote_ena",                 0x28,  19,	      32,	0xfff7ffff},
	{ "evb_irq_dpll_status_change_ena",          0x28,  20,	      32,	0xffefffff},
	{ "evb_irq_dpll_ref_change_ena",             0x28,  21,	      32,	0xffdfffff},
	{ "evb_irq_gpi_status_ena",                  0x28,  22,	      32,	0xffbfffff},
	{ "evb_irq_tdm_status_ena",                  0x28,  23,	      32,	0xff7fffff},
	{ "evb_irq_uc2_rx_avail_ena",                0x28,  24,	      32,	0xfeffffff},
	{ "evb_irq_uc2_tx_empty_ena",                0x28,  25,	      32,	0xfdffffff},
	{ "evb_irq_atpc2_local_ena",                 0x28,  26,	      32,	0xfbffffff},
	{ "evb_irq_atpc2_remote_ena",                0x28,  27,	      32,	0xf7ffffff},
	{ "evb_irq_global",                          0x28,  31,	      32,	0x7fffffff},
	{ "evb_gpi_irq",                             0x2c,  0,	      32,	0xffffff00},
	{ "evb_dbg_20_status",                       0x30,  0,	      32,	0x80000000},
	{ "evb_dbg_20_ena",                          0x30,  31,	      32,	0x7fffffff},

	{ "dpll_ver_minor",                          0x100, 	0 ,	  32,	0xffffff00},
	{ "dpll_ver_major",                          0x100, 	8 ,	  32,	0xffff00ff},
	{ "dpll_dpll_mode",                          0x104, 	0,	  32,	0xfffffff0},
	{ "dpll_dpll_state",                         0x104, 	16,	  32,	0xfff8ffff},
	{ "dpll_dpll_status_rx_1",                   0x108, 	0,	  32,	0xfffffffe},
	{ "dpll_dpll_status_rx_2",                   0x108, 	1,	  32,	0xfffffffd},
	{ "dpll_dpll_status_ref0",                   0x108, 	2,	  32,	0xfffffffb},
	{ "dpll_dpll_status_ref1",                   0x108, 	3,	  32,	0xfffffff7},
	{ "dpll_dpll_status_rcv0",                   0x108, 	6,	  32,	0xffffffbf},
	{ "dpll_dpll_status_rcv1",                   0x108, 	7,	  32,	0xffffff7f},
	{ "dpll_dpll_tx_kp",                         0x10c, 	0,	  32,	0xffffffe0},
	{ "dpll_dpll_tx_ki",                         0x10c, 	8,	  32,	0xffffe0ff},
	{ "dpll_dpll_rx_kp",                         0x10c, 	16,	  32,	0xffe0ffff},
	{ "dpll_dpll_rx_ki",                         0x10c, 	24,	  32,	0xe0ffffff},
	{ "dpll_divisor_ref0",                       0x110, 	0,	  32,	0xffff0000},
	{ "dpll_divisor_ref1",                       0x110, 	16,	  32,	0xffff    },
	{ "dpll_ext_rx_ref_divisor",                 0x118, 	0,	  32,	0xffff8000},
	{ "dpll_adj_override_ref0",                  0x11c, 	0,	  32,	0xffffff00},
	{ "dpll_adj_override_ref1",                  0x11c, 	8,	  32,	0xffff00ff},
	{ "dpll_dpll_state_change",                  0x120, 	0,	  32,	0xfffffffc},
	{ "dpll_dpll_tx_symrate_adj",                0x124, 	0,	  32,	0x0       },

	{ "tdm_ver_minor",                           0x1a0, 	0,	  32,	0xffffff00},
	{ "tdm_ver_major",                           0x1a0, 	8,	  32,	0xffff00ff},
	{ "tdm_tdm_e1_liu",                          0x1a0, 	16,	  32,	0xfffeffff},
	{ "tdm_tdm_6144",                            0x1a0, 	17,	  32,	0xfffdffff},
	{ "tdm_tdm_hdb3_loop",                       0x1a0, 	18,	  32,	0xfffbffff},
	{ "tdm_tdm_stm1_supported",                  0x1a0, 	19,	  32,	0xfff7ffff},
	{ "tdm_tdm_e1_channels",                     0x1a0, 	20,	  32,	0xffcfffff},
	{ "tdm_tdm_irq",                             0x1a0, 	24,	  32,	0xffffff  },
	{ "tdm_tdm_bytes",                           0x1a4, 	0,	  32,	0xffffe000},
	{ "tdm_tdm_test",                            0x1a4, 	13,	  32,	0xffff9fff},
	{ "tdm_tdm_pdh_mux_n",                       0x1a4, 	15,	  32,	0xff007fff},
	{ "tdm_tdm_tributaries",                     0x1a4, 	24,	  32,	0xe0ffffff},
	{ "tdm_tdm_stm1",                            0x1a4, 	30,	  32,	0xbfffffff},
	{ "tdm_tdm_vcxo",                            0x1a4, 	31,	  32,	0x7fffffff},
	{ "tdm_tdm_source_target",                   0x1a8, 	0,	  32,	0xfffff000},
	{ "tdm_tdm_sink_target",                     0x1a8, 	12,	  32,	0xff000fff},
	{ "tdm_tdm_target_coef",                     0x1a8, 	24,	  32,	0xfcffffff},
	{ "tdm_tdm_stm1_tx_s1",                      0x1a8, 	28,	  32,	0xfffffff },
	{ "tdm_tdm_status",                          0x1ac, 	0,	  32,	0xffffffc0},
	{ "tdm_tdm_vcxo_fault",                      0x1ac, 	6,	  32,	0xffffff3f},
	{ "tdm_tdm_stm1_tx_OOF",                     0x1ac, 	10,	  32,	0xfffffbff},
	{ "tdm_tdm_stm1_rx_OOF",                   	 0x1ac, 	11,	  32,	0xfffff7ff},
	{ "tdm_tdm_stm1_rx_s1",                      0x1ac, 	12,	  32,	0xffff0fff},
	{ "tdm_tdm_e1_active",                       0x1ac, 	16,	  32,	0xffff    },
	{ "tdm_tdm_vco_n",                           0x1b0, 	0,	  32,	0xffffff00},
	{ "tdm_tdm_vco_m",                           0x1b4, 	0,	  32,	0xffffff00},
	{ "tdm_tdm_vco_c0",                          0x1b8, 	0,	  32,	0xffffff00},
	{ "tdm_tdm_vco_pre",                         0x1bc, 	0,	  32,	0xfffffffc},

	{ "tme_conf_rx_pause_addr0",                 0x800, 	0,	  32,    	   0x0       },
	{ "tme_conf_rx_pause_addr1",                 0x804, 	0,	  32,		   0xffff0000},
	{ "tme_conf_rx_crc_err_disable",             0x804, 	24,	  32,		   0xfeffffff},
	{ "tme_conf_rx_len_err_disable",             0x804, 	25,	  32,		   0xfdffffff},
	{ "tme_conf_rx_half_duplex",                 0x804, 	26,	  32,		   0xfbffffff},
	{ "tme_conf_rx_vlan_enable",                 0x804, 	27,	  32,		   0xf7ffffff},
	{ "tme_conf_rx_enable",                      0x804, 	28,	  32,		   0xefffffff},
	{ "tme_conf_rx_crc_forward",                 0x804, 	29,	  32,		   0xdfffffff},
	{ "tme_conf_rx_jumbo_enable",                0x804, 	30,	  32,		   0xbfffffff},
	{ "tme_conf_rx_reset",                       0x804, 	31,	  32,		   0x7fffffff},
	{ "tme_conf_tx_ifg_adj_ena",                 0x808, 	25,	  32,		   0xfdffffff},
	{ "tme_conf_tx_half_duplex",                 0x808, 	26,	  32,		   0xfbffffff},
	{ "tme_conf_tx_vlan_enable",                 0x808, 	27,	  32,		   0xf7ffffff},
	{ "tme_conf_tx_enable",                      0x808, 	28,	  32,		   0xefffffff},
	{ "tme_conf_tx_crc_forward",                 0x808, 	29,	  32,		   0xdfffffff},
	{ "tme_conf_tx_jumbo_enable",                0x808, 	30,	  32,		   0xbfffffff},
	{ "tme_conf_tx_reset",                       0x808, 	31,	  32,		   0x7fffffff},
	{ "tme_conf_rx_flow_cntrl_ena",              0x80c, 	29,	  32,		   0xdfffffff},
	{ "tme_conf_tx_flow_cntrl_ena",              0x80c, 	30,	  32,		   0xbfffffff},
	{ "tme_conf_mac_speed",                      0x810, 	30,	  32,		   0x3fffffff},
	{ "tme_conf_rx_max_frame",                   0x814, 	0,	  32,		   0xffff8000},
	{ "tme_conf_rx_max_frame_ena",               0x814, 	16,	  32,		   0xfffeffff},
	{ "tme_conf_tx_max_frame",                   0x818, 	0,	  32,		   0xffff8000},
	{ "tme_conf_tx_max_frame_ena",               0x818, 	16,	  32,		   0xfffeffff},
	{ "tme_conf_id_patch_level",                 0x8f8, 	0,	  32,		   0xffffff00},
	{ "tme_conf_ver_minor",                      0x8f8, 	16,	  32,		   0xff00ffff},
	{ "tme_conf_ver_major",                      0x8f8, 	24,	  32,		   0xffffff  },
	{ "tme_conf_ability_10m",                    0x8fc, 	0,	  32,		   0xfffffffe},
	{ "tme_conf_ability_100m",                   0x8fc, 	1,	  32,		   0xfffffffd},
	{ "tme_conf_ability_1g",                     0x8fc, 	2,	  32,		   0xfffffffb},
	{ "tme_conf_ability_statistics",             0x8fc, 	8,	  32,		   0xfffffeff},
	{ "tme_conf_ability_hd",                     0x8fc, 	9,	  32,		   0xfffffdff},
	{ "tme_conf_ability_framefilter",            0x8fc, 	10,	  32,		   0xfffffbff},

	{ "tme_mdio_clk_div",                        0x900, 	0,	  32,		   0xffffffc0},
	{ "tme_mdio_enable",                         0x900, 	6,	  32,		   0xffffffbf},
	{ "tme_mdio_ready",                          0x904, 	7,	  32,		   0xffffff7f},
	{ "tme_mdio_initiate",                       0x904, 	11,	  32,		   0xfffff7ff},
	{ "tme_mdio_op",                             0x904, 	14,	  32,		   0xffff3fff},
	{ "tme_mdio_regad",                          0x904, 	16,	  32,		   0xffe0ffff},
	{ "tme_mdio_phyad",                          0x904, 	24,	  32,		   0xe0ffffff},
	{ "tme_mdio_wdata",                          0x908, 	0,	  32,		   0xffff0000},
	{ "tme_mdio_rdata",                          0x90c, 	0,	  32,		   0xffff0000},
	{ "tme_mdio_rdata_rdy",                      0x90c, 	16,	  32,		   0xfffeffff},

	{ "tme_stat_rx_bytes",                       0xa00, 	0,	  32,		   0x0},
	{ "tme_stat_tx_bytes",                       0xa08, 	0,	  32,		   0x0},
	{ "tme_stat_rx_usize_frames",                0xa10, 	0,	  32,		   0x0},
	{ "tme_stat_tx_usize_frames",                0xa18, 	0,	  32,		   0x0},
	{ "tme_stat_rx_64_frames",                   0xa20, 	0,	  32,		   0x0},
	{ "tme_stat_rx_65to127_frames",              0xa28, 	0,	  32,		   0x0},
	{ "tme_stat_rx_128to255_frames",             0xa30, 	0,	  32,		   0x0},
	{ "tme_stat_rx_256to511_frames",             0xa38, 	0,	  32,		   0x0},
	{ "tme_stat_rx_512to1023_frames",            0xa40, 	0,	  32,		   0x0},
	{ "tme_stat_rx_1024toMax_frames",            0xa48, 	0,	  32,		   0x0},
	{ "tme_stat_rx_osize_frames",                0xa50, 	0,	  32,		   0x0},
	{ "tme_stat_tx_64_frames",                   0xa58, 	0,	  32,		   0x0},
	{ "tme_stat_tx_65to127_frames",              0xa60, 	0,	  32,		   0x0},
	{ "tme_stat_tx_128to255_frames",             0xa68, 	0,	  32,		   0x0},
	{ "tme_stat_tx_256to511_frames",             0xa70, 	0,	  32,		   0x0},
	{ "tme_stat_tx_512to1023_frames",            0xa78, 	0,	  32,		   0x0},
	{ "tme_stat_tx_1024toMax_frames",            0xa80, 	0,	  32,		   0x0},
	{ "tme_stat_tx_osize_frames",                0xa88, 	0,	  32,		   0x0},
	{ "tme_stat_rx_good_frames",                 0xa90, 	0,	  32,		   0x0},
	{ "tme_stat_rx_crc_errors",                  0xa98, 	0,	  32,		   0x0},
	{ "tme_stat_rx_bcast_frames",                0xaa0, 	0,	  32,		   0x0},
	{ "tme_stat_rx_mcast_frames",                0xaa8, 	0,	  32,		   0x0},
	{ "tme_stat_rx_cntrl_frames",                0xab0, 	0,	  32,		   0x0},
	{ "tme_stat_rx_len_errors",                  0xab8, 	0,	  32,		   0x0},
	{ "tme_stat_rx_vlan_frames",                 0xac0, 	0,	  32,		   0x0},
	{ "tme_stat_rx_pause_frames",                0xac8, 	0,	  32,		   0x0},
	{ "tme_stat_rx_op_errors",                   0xad0, 	0,	  32,		   0x0},
	{ "tme_stat_tx_good_frames",                 0xad8, 	0,	  32,		   0x0},
	{ "tme_stat_tx_bcast_frames",                0xae0, 	0,	  32,		   0x0},
	{ "tme_stat_tx_mcast_frames",                0xae8, 	0,	  32,		   0x0},
	{ "tme_stat_tx_urun_errors",                 0xaf0, 	0,	  32,		   0x0},
	{ "tme_stat_tx_cntrl_frames",                0xaf8, 	0,	  32,		   0x0},
	{ "tme_stat_tx_vlan_frames",                 0xb00, 	0,	  32,		   0x0},
	{ "tme_stat_tx_pause_frames",                0xb08, 	0,	  32,		   0x0},
	{ "tme_stat_tx_single_collisions",           0xb10, 	0,	  32,		   0x0},
	{ "tme_stat_tx_multi_collisions",            0xb18, 	0,	  32,		   0x0},
	{ "tme_stat_tx_deferred",                    0xb20, 	0,	  32,		   0x0},
	{ "tme_stat_tx_late_collisions",             0xb28, 	0,	  32,		   0x0},
	{ "tme_stat_tx_excess_collisions",           0xb30, 	0,	  32,		   0x0},
	{ "tme_stat_tx_excess_deferral",             0xb38, 	0,	  32,		   0x0},
	{ "tme_stat_tx_align_errors",                0xb40, 	0,	  32,		   0x0},

	{ "eth_ver_minor",                           0xc00, 	0,	  32,		   0xffffff00},
	{ "eth_ver_major",                           0xc00, 	8,	  32,		   0xffff00ff},
	{ "eth_rx_fifo_len",                         0xc04, 	0,	  32,		   0x0       },
	{ "eth_tx_fifo_len",                         0xc08, 	0,	  32,		   0x0       },
	{ "eth_xon_thres",                           0xc0c, 	0,	  32,		   0xffff0000},
	{ "eth_xoff_thres",                          0xc10, 	0,	  32,		   0xffff0000},
	{ "eth_xoff_timeout",                        0xc14, 	0,	  32,		   0xfffffc00},
	{ "eth_xon_xoff_dis",                        0xc14, 	31,	  32,		   0x7fffffff},
	{ "eth_eth_local_loopback",                  0xc18, 	0,	  32,		   0xfffffffe},
	{ "eth_eth_lb_ena_override",                 0xc18, 	1,	  32,		   0xfffffffd},
	{ "eth_pause_quanta",                        0xc1c, 	0,	  32,		   0xffff0000},
	{ "eth_rx_detected",                         0xc20, 	0,	  32,		   0x0       },
	{ "eth_rx_dropped",                          0xc24, 	0,	  32,		   0x0       },
	{ "eth_tx_detected",                         0xc28, 	0,	  32,		   0x0       },
	{ "eth_tx_dropped",                          0xc2c, 	0,	  32,		   0x0       },

	{ "tx_info",                                 0x4000,	0,	  32,		   0xffff0000},
	{ "tx_ver_minor",                            0x4000,	16,	  32,		   0xff00ffff},
	{ "tx_ver_major",                            0x4000,	24,	  32,		   0xffffff  },
	{ "tx_pilot_level",                          0x4004,	0,	  32,		   0xffffff00},
	{ "tx_pilot_only_ena",                       0x4004,	8,	  32,		   0xfffffeff},
	{ "tx_scrambler_seed",                       0x4004,	16,	  32,		   0xfffeffff},
	{ "tx_symrate",                              0x4008,	0,	  32,		   0xffffff00},
	{ "tx_mod_indx",                             0x4008,	8,	  32,		   0xfffff0ff},
	{ "tx_ilv_depth",                            0x4008,	16,	  32,		   0xff00ffff},
	{ "tx_wayside_len",                          0x4008,	24,	  32,		   0xffffffL },
	{ "tx_fec_cw_len",                           0x400c,	0,	  32,		   0xffffff00},
	{ "tx_fec_pl_len",                           0x400c,	8,	  32,		   0xffff00ff},
	{ "tx_fec_override_ena",                     0x400c,	16,	  32,		   0xfffeffff},
	{ "tx_symadjust",                            0x4010,	8,	  32,		   0xff      },
	{ "tx_dpd_sqr_a",                            0x4014,	0,	  32,		   0xfffffffc},
	{ "tx_dpd_sqr_p",                            0x4014,	4,	  32,		   0xffffff0f},
	{ "tx_dpd_dbg_level_a",                      0x4014,	8,	  32,		   0xffff00ff},
	{ "tx_dpd_dbg_level_p",                      0x4014,	16,	  32,		   0xff00ffff},
	{ "tx_dpd_enable",                           0x4014,	24,	  32,		   0xfeffffff},
	{ "tx_dpd_status_lock",                      0x4014,	25,	  32,		   0xfdffffff},
	{ "tx_dpd_status_a",                         0x4018,	0,	  32,		   0xffffff00},
	{ "tx_dpd_status_p",                         0x4018,	8,	  32,		   0xffff00ff},
	{ "tx_dpd_override_a",                       0x4018,	16,	  32,		   0xff00ffff},
	{ "tx_dpd_override_p",                       0x4018,	24,	  32,		   0xffffffL },
	{ "tx_dc_offset_i",                          0x401c,	0,	  32,		   0xffff0000},
	{ "tx_dc_offset_q",                          0x401c,	16,	  32,		   0xffff    },
	{ "tx_awgn_scaler",                          0x4020,	0,	  32,		   0xffff0000},
	{ "tx_attenuate",                            0x4024,	0,	  32,		   0xfffffff0},
	{ "tx_lsb_mask_sel",                         0x4024,	8,	  32,		   0xfffff8ff},
	{ "tx_lo_dds_freq",                          0x4028,	0,	  32,		   0x0       },
	{ "tx_dbg_sqsig_load",                       0x402c,	0,	  32,		   0xffff8000},
	{ "tx_dbg_mls_len",                          0x4030,	0,	  32,		   0xfffffffc},
	{ "tx_dbg_mls_mode",                         0x4030,	4,	  32,		   0xffffff0f},
	{ "tx_dbg_lo_out_ena",                       0x4030,	8,	  32,		   0xfffffeff},
	{ "tx_dbg_bert_ena",                         0x4030,	16,	  32,		   0xfffeffff},
	{ "tx_dbg_zeroes_ena",                       0x4030,	17,	  32,		   0xfffdffff},
	{ "tx_dac_i_delay_ena",                      0x4034,	0,	  32,		   0xfffffffe},
	{ "tx_dac_q_delay_ena",                      0x4034,	1,	  32,		   0xfffffffd},
	{ "tx_dac_iq_swap",                          0x4034,	2,	  32,		   0xfffffffb},
	{ "tx_dpd_override_ena",                     0x4034,	3,	  32,		   0xfffffff7},
	{ "tx_coef_data",                            0x4034,	8,	  32,		   0xffff00ff},
	{ "tx_coef_addr",                            0x4034,	16,	  32,		   0xffff    },
	{ "tx_coef_addr_data",                       0x4038,	0,	  32,		   0x0       },

	{ "rx_hw_xpic_eq",                           0x4100,	1,	  32,		   0xfffffffd},
	{ "rx_hw_ifsampling_mode",                   0x4100,	2,	  32,		   0xfffffffb},
	{ "rx_ver_minor",                            0x4100,	16,	  32,		   0xff00ffff},
	{ "rx_ver_major",                            0x4100,	24,	  32,		   0xffffff  },
	{ "rx_lo_dds_freq",                          0x4104,	8,	     32,	       0xff      },              
	{ "rx_symrate",                              0x4108,	0,	     32,	       0xffffff00},              
	{ "rx_mod_indx",                             0x4108,	8,	     32,	       0xfffff0ff},              
	{ "rx_ilv_depth",                            0x4108,	16,	     32,	       0xff00ffff},              
	{ "rx_wayside_len",                          0x4108,	24,	     32,	       0xffffff  },              
	{ "rx_fec_cw_len",                           0x410c,	0,	     32,	       0xffffff00},              
	{ "rx_fec_pl_len",                           0x410c,	8,	     32,	       0xffff00ff},              
	{ "rx_fec_override_ena",                     0x410c,	16,	     32,	       0xfffeffff},              
	{ "rx_symadjust",                            0x4110,	8,	     32,	       0xff      },              
	{ "rx_dde_step",                             0x4114,	0,	     32,  		   0xffffffc0},                  
	{ "rx_dde_qpsk_zf_ena",                      0x4114,	6,	     32,  		   0xffffffbf},                  
	{ "rx_dde_zf_reset",                         0x4114,	7,	     32,  		   0xffffff7f},
	{ "rx_agc1_gain_override",                   0x4114,	8,	     32,  		   0xffff00ff},
	{ "rx_agc1_gain_invert",                     0x4114,	16,	     32,		   0xfffeffff},
	{ "rx_scrambler_seed",                       0x4114,	17,	     32,  		   0xfffdffff},
	{ "rx_corr_disable",                         0x4114,	18,	     32,  		   0xfffbffff},
	{ "rx_corr_dc_ofs_i",                        0x4118,	0,	     32,  		   0xffffff00},
	{ "rx_corr_dc_ofs_q",                        0x4118,	8,	     32,  		   0xffff00ff},
	{ "rx_corr_iq_magnitude",                    0x4118,	16,	     32,  		   0xff00ffff},
	{ "rx_corr_iq_phase",                        0x4118,	24,	     32,  		   0xffffff  },
	{ "rx_corr_gdelay",                          0x411c,	0,	     32,  		   0xffffffe0},
	{ "rx_coef_data",                            0x411c,	8,	     32,  	       0xffff00ff},
	{ "rx_coef_addr",                            0x411c,	16,	     32,  		   0xffff    },
	{ "rx_agc1_adj",                             0x4120,	0, 	     32,  		   0xfffffff8},
	{ "rx_agc1_thres_adj",                       0x4120,	8, 	     32,		   0xffff00ff},
	{ "rx_agc2_adj",                             0x4120,	16,	     32,		   0xfff8ffff},
	{ "rx_agc2_thres",                           0x4120,	24,	     32,	       0xffffff  },
	{ "rx_ccr_delay_adj",                        0x4124,	0,	     32,	       0xffffff00},
	{ "rx_ccr_pilot_delay_adj",                  0x4124,	8,	     32,	       0xffff00ff},
	{ "rx_ccr_pid_p",                            0x4124,	16,	     32,	       0xff00ffff},
	{ "rx_ccr_pid_i",                            0x4124,	24,	     32,	       0xffffff  },
	{ "rx_fcr_adj",                              0x4128,	0, 	     32,	       0xffffff00},
	{ "rx_fcr_rc_adj",                           0x4128,	8, 	     32,	       0xfffff8ff},
	{ "rx_fcr_cffk_ofs",                         0x4128,	16,	     32,	       0xff00ffff},
	{ "rx_pdc_fmixer",                           0x412c,	0, 	     32,	       0xffffff00},
	{ "rx_pdc_pmixer",                           0x412c,	8, 	     32,		   0xffff00ff},
	{ "rx_sr_pid_p",                             0x4130,	0, 	     32,		   0xfffffffc},
	{ "rx_sr_pid_i",                             0x4130,	8,	     32,		   0xfffff8ff},
	{ "rx_sr_pid_adj",                           0x4130,	16,	     32,		   0xff00ffff},
	{ "rx_alarm_loc",                            0x4134,	0,	     32,		   0xfffffffe},
	{ "rx_alarm_hde",                            0x4134,	1,	     32,		   0xfffffffd},
	{ "rx_alarm_sde",                            0x4134,	2,	     32,		   0xfffffffb},
	{ "rx_dist_err_a",                           0x4138,	0,	     32,		   0xffffff00},
	{ "rx_dist_err_p",                           0x4138,	16,	     32,		   0xff00ffff},
	{ "rx_mse_avg",                              0x413c,	0,	     32,		   0xffffff00},
	{ "rx_ber_avg",                              0x413c,	8,	     32,		   0xffff00ff},
	{ "rx_agc1_gain",                            0x4140,	0,	     32,		   0xfffc0000},
	{ "rx_dbg_rx",                               0x4144,	0,	     32,	       0xffffff00},
	{ "rx_dbg_fdet_ofs",                         0x4144,	8,	     32,		   0xffff00ff},
	{ "rx_dbg_agc1_step_ena",                    0x4144,	16,	     32,		   0xfffeffff},
	{ "rx_dbg_agc2_step_ena",                    0x4144,	17,	     32,		   0xfffdffff},
	{ "rx_dbg_sr_step_ena",                      0x4144,	18,	     32,		   0xfffbffff},
	{ "rx_dbg_sr_csr_force",                     0x4144,	19,	     32,	       0xfff7ffff},
	{ "rx_dbg_rx_mask",                          0x4144,	24,	     32,	       0xf0ffffff},
	{ "rx_digital_loop_ena",                     0x4148,	0, 	     32,	       0xfffffffe},
	{ "rx_dsp_data_loop_ena",                    0x4148,	1, 	     32,	       0xfffffffd},
	{ "rx_adc_i_delay_ena",                      0x4148,	8,	     32,	       0xfffffeff},
	{ "rx_adc_q_delay_ena",                      0x4148,	9,	     32,	       0xfffffdff},
	{ "rx_adc_iq_swap",                          0x4148,	10,	     32,	       0xfffffbff},
	{ "rx_bert_uncoded",                         0x414c,	0,	     32,	       0x0       },
	{ "rx_bert_coded",                           0x4150,	0,	     32,	       0x0       },
	{ "rx_bert_coded_errors",                    0x4154,	0,	     32,		   0x0       },
	{ "rx_xpic_enable",                          0x4158,	0,	     32,		   0xfffffffe},
	{ "rx_xpic_eq_reset",                        0x4158,	1,	     32,		   0xfffffffd},
	{ "rx_xpic_eq_step",                         0x4158,	8,	     32,		   0xfffff0ff},
	{ "rx_xpic_eq_delay",                        0x4158,	16,	     32,		   0xff80ffff},
	{ "rx_xpic_level",                           0x415c,	0,	     32,		   0xffff0000},
	{ "rx_uncoded_window_count",                 0x415c,	16,	     32,		   0xffff    },
	{ "rx_dbg_scope_sqwave",                     0x4160,	0,	     32,		   0xffff0000},
	{ "rx_coded_window_count",                   0x4160,	16,	     32,		   0xffff    },
	{ "rx_coded_nrwin",                          0x4164,	0,	     32,		   0xffffffc0},
	{ "rx_ber_zeroes",                           0x4164,	7,	     32,		   0xffffff7f},
	{ "rx_alarm_loc_irq_ena",                    0x416c,	0,	     32,		   0xfffffffe},
	{ "rx_alarm_hde_irq_ena",                    0x416c,	1,	     32,	       0xfffffffd},
	{ "rx_alarm_sde_irq_ena",                    0x416c,	2,	     32,		   0xfffffffb},
	{ "rx_alarm_state",                          0x4170,	0,	     32,		   0xfffffff8},
	{ "rx_corr_txiq_phase",                      0x4174,	0,	     32,		   0xffffff00},
	{ "rx_corr_txiq_magnitude",                  0x4174,	8,	     32,		   0xff0000ff},
	{ "rx_rx_freq_offset",                       0x4174,	16,	     32,	       0xffff    },
	{ "rx_corr_txdc_ofs_i",                      0x4178,	0,	     32,	       0xffff0000},
	{ "rx_corr_txdc_ofs_q",                      0x4178,	16,	     32,	       0xffff    },

	{ "acm_info",                                0x7000,	0,	     32,	       0xffff0000},
	{ "acm_version",                             0x7000,	16,	     32,	       0xffff    },
	{ "acm_acm_enable",                          0x7004,	0,	     32,	       0xfffffffe},
	{ "acm_force_state",                         0x7004,	1,	     32,	       0xfffffffd},
	{ "acm_acm_update_enable",                   0x7004,	2,	     32,	       0xfffffffb},
	{ "acm_acm_ber_based",                       0x7004,	3,	     32,		   0xfffffff7},
	{ "acm_state_low_index",                     0x7004,	8,	     32,		   0xffff00ff},
	{ "acm_state_high_index",                    0x7004,	16,	     32,		   0xff00ffff},
	{ "acm_disabled_state",                      0x7004,	24,	     32,		   0xffffff  },
	{ "acm_forced_state",                        0x7008,	0,	     32,		   0xffffff00},
	{ "acm_svc_ch_exlen",                        0x7008,	8,	     32,		   0xffff00ff},
	{ "acm_tx_acm_state",                        0x700c,	0,	     32,		   0xffffff00},
	{ "acm_rx_acm_state",                        0x700c,	8,	     32,		   0xffff00ff},
	{ "acm_rx_mse_avg",                          0x700c,	16,	     32,		   0xff00ffff},
	{ "acm_fec_broken_packets",                  0x700c,	24,	     32,		   0xffffff  },
	{ "acm_atpc_mse_low",                        0x7010,	0,	     32,		   0xffffff00},
	{ "acm_atpc_mse_high",                       0x7010,	8,	     32,		   0xffff00ff},
	{ "acm_atpc_local_state",                    0x7010,	16,	     32,	       0xfffcffff},
	{ "acm_atpc_remote_state",                   0x7010,	18,	     32,		   0xfff3ffff},
	{ "acm_atpc_state_change_irq",               0x7014,	0,	     32,		   0xfffffffc},

	{ "acm_usr_rx_fifo_data",                    0x7400,	0,	     32,		   0xffffff00},
	{ "acm_usr_rx_fifo_cnt",                     0x7400,	8,	     32,	       0xffff00ff},
	{ "acm_usr_rx_fifo_full",                    0x7400,	16,	     32,	       0xfffeffff},
	{ "acm_usr_rx_channel_fail",                 0x7400,	18,	     32,	       0xfffbffff},
	{ "acm_usr_tx_fifo_avail",                   0x7404,	0,	     32,	       0xffffff00},
	{ "acm_usr_tx_fifo_data",                    0x7408,	0,	     32,	       0x0       },
};


static void pabort(const char *s)
{
    perror(s);
    abort();
}

static const char *device = "/dev/spidev1.1";
static uint8_t mode;
static uint8_t bits = 8;
//static uint32_t speed = 500000;
static uint32_t speed = 20000000;
static uint16_t delay;

int fdFpga;

static void print_usage(const char *prog)
{
    printf("Usage: %s [-DsbdlHOLC3]\n", prog);
    puts(" -D --device device to use (default /dev/spidev1.1)\n"
     " -s --speed max speed (Hz)\n"
     " -d --delay delay (usec)\n"
     " -b --bpw bits per word \n"
     " -l --loop loopback\n"
     " -H --cpha clock phase\n"
     " -O --cpol clock polarity\n"
     " -L --lsb least significant bit first\n"
     " -C --cs-high chip select active high\n"
     " -3 --3wire SI/SO signals shared\n");
    exit(1);
}

static void parse_opts(int argc, char *argv[])
{
    while (1) {
        static const struct option lopts[] = {
            { "device", 1, 0, 'D' },
            { "speed", 1, 0, 's' },
            { "delay", 1, 0, 'd' },
            { "bpw", 1, 0, 'b' },
            { "loop", 0, 0, 'l' },
            { "cpha", 0, 0, 'H' },
            { "cpol", 0, 0, 'O' },
            { "lsb", 0, 0, 'L' },
            { "cs-high", 0, 0, 'C' },
            { "3wire", 0, 0, '3' },
            { "no-cs", 0, 0, 'N' },
            { "ready", 0, 0, 'R' },
            { NULL, 0, 0, 0 },
        };
        int c;

        c = getopt_long(argc, argv, "D:s:d:b:lHOLC3NR", lopts, NULL);

        if (c == -1)
            break;

        switch (c) {
        case 'D':
            device = optarg;
            break;
        case 's':
            speed = atoi(optarg);
            break;
        case 'd':
            delay = atoi(optarg);
            break;
        case 'b':
            bits = atoi(optarg);
            break;
        case 'l':
            mode |= SPI_LOOP;
            break;
        case 'H':
            mode |= SPI_CPHA;
            break;
        case 'O':
            mode |= SPI_CPOL;
            break;
        case 'L':
            mode |= SPI_LSB_FIRST;
            break;
        case 'C':
            mode |= SPI_CS_HIGH;
            break;
        case '3':
            mode |= SPI_3WIRE;
            break;
        default:
            print_usage(argv[0]);
            break;
        }
    }
}

//发送信息 与spi交互
static int tranfer_msg(int fd, uint8_t *tx, uint8_t *rx, int len)
{
	//memset(rx, 0, len);
	//printf("\ntranfer_msg len:%d", len);

	int ret;
	struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)tx,
		.rx_buf = (unsigned long)rx,
		.len = len,
		.delay_usecs = delay,
		.speed_hz = 0,
		.bits_per_word = 0,
		.cs_change = 0,
	};
	
	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
	
	if (ret == 1){ perror("SPI_IOC_MESSAGE"); return -1;}
#if 0
	printf("\nioctl SPI_IOC_MESSAGE ret:%d", ret);

	hex_dump("tx", tx, len);
	hex_dump("rx", rx, len);
	printf("\n");
#endif
	return 0;
}

static int fpga_write(unsigned int addr, uint8_t *buf)
{
	uint8_t tx_buf[] = {0x08, 
		buf[0], buf[1], buf[2], buf[3], 
		((addr>>24)&0xff), ((addr>>16)&0xff), ((addr>>8)&0xff), ((addr)&0xff),
		0x0f};
	return tranfer_msg(fdFpga, tx_buf, 0, sizeof(tx_buf));
}

static int fpga_write_uint32(uint32_t addr, uint32_t data)
{
	uint8_t buf[] = {
		((data>>24)&0xff), ((data>>16)&0xff), ((data>>8)&0xff), ((data)&0xff)};
	return fpga_write(addr, buf);
}

//读取spi数据到buf
static int fpga_read(unsigned int addr, uint8_t *buf)
{
	int i, len = 4;
	uint8_t tx_buf[32] = {0};
	uint8_t rx_buf[32] = {0};

	//printf("\nfpga_read(0x%x):", addr);

	tx_buf[0] = 0x4;
	tx_buf[1] = (addr>>24) & 0xff;
	tx_buf[2] = (addr>>16) & 0xff;
	tx_buf[3] = (addr>>8) & 0xff;
	tx_buf[4] = addr & 0xff;
	tx_buf[5] = 0x8f;
	tranfer_msg(fdFpga, tx_buf, rx_buf, 6);
	
	for (i = 0; i < 10; i ++)
	{
		tx_buf[0] = 0x8f; tx_buf[1] = 0xff;
		tranfer_msg(fdFpga, tx_buf, rx_buf, 2);
		if ((rx_buf[1] & 0x01) == 0x00) break;
		usleep(100);
	}

	if ((rx_buf[1] & 0x01) != 0x00) return -1;

	//usleep(500);
	tx_buf[0]=0x8e;
	for (i = 0; i < len; i++)
		tx_buf[1+i] = 0xff;
	tranfer_msg(fdFpga, tx_buf, rx_buf, len+1);

	memcpy(buf, rx_buf+1, len);
	//printf("\nfpga_read end.");
	return 0;
}
//read 32位
static uint32_t fpga_read_uint32(uint32_t addr)
{
	uint32_t rd = -1u;
	uint8_t buf[8] = {0};
	if (0 == fpga_read(addr, buf)){
		rd = ((buf[0]<<24) | (buf[1]<<16) | (buf[2]<<8) | buf[3]);
	}
	//printf("fpga_read_uint32 R(0x%x)=0x%08x\n", addr, rd);
	return rd;
}


//读寄存器

static uint32_t rReg(char name[])
{
	int i, itmp;
	uint32_t rdata;
	//寻找寄存器数组
	for( i = 0; i < (sizeof(reg_r)/sizeof(tmp)); i++)
	{
		if( strcmp( name, (char *)reg_r[i].name) == 0)
		{
			itmp = i;
			break;
		}
	}
	//printf("sizeof(reg_r)=%d,sizeof(tmp)=%d,sizeof(reg_r)/sizeof(tmp)=%d", sizeof(reg_r), sizeof(tmp), sizeof(reg_r)/sizeof(tmp));
	if(i == (sizeof(reg_r)/sizeof(tmp)))
	{
		printf("no this register\n");
		return -1;
	}
	//printf("reg_r[%d]  addr_base 0x%x  addr_offset %d  width %d  mask 0x%x  mane %s\n", itmp, reg_r[itmp].addr_base, reg_r[itmp].addr_offset, reg_r[itmp].width, reg_r[itmp].mask, reg_r[itmp].name);
	
	uint32_t src_data = fpga_read_uint32( reg_r[itmp].addr_base);
	
	//printf("R(0x%x)=0x%08x\n", reg_r[itmp].addr_base, src_data);
	
	rdata = ((src_data & ( ~( reg_r[itmp].mask))) >> reg_r[itmp].addr_offset);
	
	return rdata;
}

#if 1
static int wReg(char name[], int wdata)
{
	int i, itmp;
	//寻找寄存器数组
	for( i=0; i < (sizeof(reg_r)/sizeof(tmp)); i++)
	{
		if( strcmp( name, (char *)reg_r[i].name) == 0)
		{
			itmp = i;
			break;
		}
	}
	if(i == (sizeof(reg_r)/sizeof(tmp)))
	{
		printf("no this register\n");
		return -1;
	}
	printf("reg_r[%d]  addr_base 0x%x  addr_offset %d  width %d  mask 0x%x  mane %s\n", itmp, reg_r[itmp].addr_base, reg_r[itmp].addr_offset, reg_r[itmp].width, reg_r[itmp].mask, reg_r[itmp].name);
	
	uint32_t src_data = fpga_read_uint32( reg_r[itmp].addr_base);
	
	printf("R(0x%x)=0x%08x\n", reg_r[itmp].addr_base, src_data);
	
	uint32_t w_data = (( src_data & reg_r[itmp].mask) | (wdata << reg_r[itmp].addr_offset));
	printf("w_data=0x%08x\n", w_data);
	
	fpga_write_uint32( reg_r[itmp].addr_base, w_data);
	
	src_data = fpga_read_uint32( reg_r[itmp].addr_base);
	
	printf("R(0x%x)=0x%08x\n", reg_r[itmp].addr_base, src_data);
	return 0;
}
#endif
//判断spi状态 等待就绪
static inline int fpga_spi_status_ready()
{
	int i;
	uint8_t tx_buf[2] = {0};
	uint8_t rx_buf[2] = {0};

	for (i = 0; i < 500; i ++)
	{
		tx_buf[0] = 0x8f; tx_buf[1] = 0xff;
		tranfer_msg(fdFpga, tx_buf, rx_buf, 2);
		if ((rx_buf[1] & 0x01) == 0x00) break;
		usleep(10);
	}

	//printf("fpga_spi_status_ready 0x%x\n", rx_buf[1]);
	return ((rx_buf[1] & 0x01) == 0x00) ? 1 : 0;
}

//把地址写入spi
static inline int fpga_spi_write_addr(uint32_t addr)
{
	uint8_t tx_buf[8] = {0};

	tx_buf[0] = 0x04;
	tx_buf[1] = (addr>>24) & 0xff;
	tx_buf[2] = (addr>>16) & 0xff;
	tx_buf[3] = (addr>>8) & 0xff;
	tx_buf[4] = addr & 0xff;
	tx_buf[5] = 0x8f;
	return tranfer_msg(fdFpga, tx_buf, NULL, 6);
}

//读取spi中的数据
static inline uint32_t fpga_spi_read_uint32()
{
	uint8_t tx_buf[8] = {0x8e, 0xff};
	uint8_t rx_buf[8] = {0}, *buf = rx_buf+1;

	tranfer_msg(fdFpga, tx_buf, rx_buf, 5);
	return ((buf[0]<<24) | (buf[1]<<16) | (buf[2]<<8) | buf[3]);
}

static inline uint32_t fpga_spi_read_uint32_2()
{
	uint8_t tx_buf[8] = {0x8e, 0xff};
	uint8_t rx_buf[8] = {0}, *buf = rx_buf+1;

	tranfer_msg(fdFpga, tx_buf, rx_buf, 5);
	return ((buf[3]<<24) | (buf[2]<<16) | (buf[1]<<8) | buf[0]);
}

//spi 读取FPGA中的数据1024
static int fpga_read_scope(int addr, uint32_t *buf)
{
	int i;
#if DEBUG
	printf("fpga_read_scope addr=0x%x\n", addr);
#endif
	fpga_spi_status_ready();
	//while (fpga_spi_read_status() == 0);
	//printf("fpga_spi_read_status sucess\n");

	uint32_t status;
	
	i = 0;
	do{
		//fpga_spi_rewrite_addr();
		fpga_spi_write_addr(addr);
		fpga_spi_status_ready();
		status = fpga_spi_read_uint32();
		i++;
		printf("status 0x%x i=%d\n", status, i);
		if(i > 100)
		{
			printf("fpga_spi_status_ready error\n");
			return -1;
		}
	}while (!(status&0x1));
	
	debug_pos();
	for (i = 0; i < 1024; i ++){
		fpga_spi_write_addr(addr+4);
		fpga_spi_status_ready();
		buf[i] = fpga_spi_read_uint32_2();

		//printf("buf[%d]=0x%08x\n", i, buf[i]);
	}

	
	return 0;
}

//fft 算法

typedef struct COMPLEX_S{
	double re, im;
}Complex;

/*********************************/
void conjugate_complex(int n,Complex in[],Complex out[])
{  
	int i = 0;  
	for(i=0;i<n;i++)  
	{  
		out[i].im = -in[i].im;  
		out[i].re = in[i].re;  
	}   
}  

void c_abs(Complex f[],float out[],int n)  
{  
	int i = 0;  
	float t;  
	for(i=0;i<n;i++)  
	{  
		t = f[i].re * f[i].re + f[i].im * f[i].im;  
		out[i] = sqrt(t);  
	}   
}  

void c_plus(Complex a,Complex b,Complex *c)  
{  
	c->re = a.re + b.re;  
	c->im = a.im + b.im;  
}  

void c_sub(Complex a,Complex b,Complex *c)  
{  
	c->re = a.re - b.re;  
	c->im = a.im - b.im;   
}  

void c_mul(Complex a,Complex b,Complex *c)  
{  
	c->re = a.re * b.re - a.im * b.im;  
	c->im = a.re * b.im + a.im * b.re;     
}  

void c_div(Complex a,Complex b,Complex *c)  
{  
	c->re = (a.re * b.re + a.im * b.im)/(b.re * b.re +b.im * b.im);  
	c->im = (a.im * b.re - a.re * b.im)/(b.re * b.re +b.im * b.im);  
}  

#define SWAP(a,b)  tempr=(a);(a)=(b);(b)=tempr  

void Wn_i(int n,int i,Complex *Wn,char flag)  
{  
	Wn->re = cos(2*PI*i/n);  
	if(flag == 1)  
		Wn->im = -sin(2*PI*i/n);  
	else if(flag == 0)  
		Wn->im = -sin(2*PI*i/n);  
}  


void fft(int N,Complex f[])  
{  
	Complex t,wn;  
	int i,j,k,m,n,l,r,M;  
	int la,lb,lc;  
	/*----计算分解的级数M=log2(N)----*/  
	for(i=N,M=1;(i=i/2)!=1;M++);   
	/*----按照倒位序重新排列原信号----*/  
	for(i=1,j=N/2;i<=N-2;i++)  
	{  
		if(i<j)  
		{  
			t=f[j];  
			f[j]=f[i];  
			f[i]=t;  
		}  
		k=N/2;  
		while(k<=j)  
		{  
			j=j-k;  
			k=k/2;  
		}  
		j=j+k;  
	}  

	/*----FFT算法----*/  
	for(m=1;m<=M;m++)  
	{  
		la=pow(2,m);       
		lb=la/2;    

		/*----碟形运算----*/  
		for(l=1;l<=lb;l++)  
		{  
			r=(l-1)*pow(2,M-m);     
			for(n=l-1;n<N-1;n=n+la) 
			{  
				lc=n+lb;         
				Wn_i(N,r,&wn,1);  
				c_mul(f[lc],wn,&t); 
				c_sub(f[n],t,&(f[lc])); 
				c_plus(f[n],t,&(f[n]));  
			}  
		}  
	}  
}  


void ifft(int N,Complex f[])  
{  
	int i=0;  
	conjugate_complex(N,f,f);  
	fft(N,f);  
	conjugate_complex(N,f,f);  
	for(i=0;i<N;i++)  
	{  
		f[i].im = (f[i].im)/N;  
		f[i].re = (f[i].re)/N;  
	}  
} 
/*************************************/

int currentFrame = 0;
static double spi_fft(uint32_t *currentData)
{
	double spectrumAverage[RX_BUF_SIZE_DIV4] = {0.0f};
	double spectrumAverages[16][RX_BUF_SIZE_DIV4] = {0.0f};
	Complex jh[RX_BUF_SIZE_DIV4];
	//int currentData[RX_BUF_SIZE_DIV2] = {0};
	int hann_windowOn  = 1;
	int averagingFrameCount = 1024;
	double wind_hann[RX_BUF_SIZE_DIV4];


	int i;

#if 0
	srand(time(NULL));
	for (i = 0; i < RX_BUF_SIZE_DIV4; i+=2){
		//currentData[i] = (rand()%4096);
		currentData[i] = 4096.0 * cos(2.0*PI*i/1000.0);// (rand()%4096);
		currentData[i+1] = 4096.0 * sin(2.0*PI*i/1000.0);// (rand()%4096);


	//	printf("\t %d", currentData[i]);
	}
#endif
		
	for (i = 0; i < 1024; i ++) {
		jh[i].re = (currentData[i] & 0xffff);
		jh[i].im = ((currentData[i]>>16) & 0xffff);//currentData[i + 1];
		if( jh[i].re > pow(2,15)) jh[i].re -= pow(2,16);
		if( jh[i].im > pow(2,15)) jh[i].im -= pow(2,16);
	}

	for (i = 0; i < RX_BUF_SIZE_DIV4; i++) 
		wind_hann[i] = hann_windowOn ? 0.5  * (1 - cos((2 * 3.1415926535 * i) / (RX_BUF_SIZE_DIV4 - 1))) : 1.0;

	double koef_12bits = 1.0;
	for (i = 0; i < RX_BUF_SIZE_DIV4; i++) {
		jh[i].re = jh[i].re * koef_12bits * wind_hann[i];
		jh[i].im = jh[i].im * koef_12bits * wind_hann[i];
	}

	
#if 0
	printf("fft before:\n");
	for (i = 0; i < RX_BUF_SIZE_DIV4; i++)
		printf("\t %lf", jh[i]);
	printf("end\n\n");
#endif
	fft(1024, jh);
	
#if 0
	printf("fft after:\n");
	for (i = 0; i < RX_BUF_SIZE_DIV4; i++)
		printf("\t %lf", jh[i]);
	printf("end\n\n");
#endif

	for (i = 0; i < RX_BUF_SIZE_DIV4; i++)
		spectrumAverage[i] = 0.0f;

	for (i = 0; i < RX_BUF_SIZE_DIV8; i++) {
		double jooksev_m_sag = sqrt(jh[i + 512].re * jh[i + 512].re + jh[i + 512].im * jh[i + 512].im);
		double jooksev_p_sag = sqrt(jh[i].re * jh[i].re + jh[i].im * jh[i].im);
		spectrumAverage[i] += jooksev_m_sag - spectrumAverages[currentFrame][i];
		spectrumAverage[i + 512] += jooksev_p_sag - spectrumAverages[currentFrame][i + 512];
		//printf("\t spectrumAverage[%d]=%lf, spectrumAverage[%d]=%lf", i, spectrumAverage[i],i+512, spectrumAverage[i+512]);
		spectrumAverages[currentFrame][i] = jooksev_m_sag;
		spectrumAverages[currentFrame][i + 512] = jooksev_p_sag;
	}

	double sum = 0.0, y1 = 20.0 * log10(spectrumAverage[0] / averagingFrameCount);
	//if (!_finite(y1)) y1 = 0;
	for (i = 1; i < RX_BUF_SIZE_DIV4; i++) {
		double y = 20.0 * log10(spectrumAverage[i] / averagingFrameCount);
		//if (!_finite(y)) y = 0;
		y1 = y;
		sum += spectrumAverage[i] / averagingFrameCount;
	}

	double retv = 20.0 * log10(sum) - 101.6;
	printf("sum %f, retv %lf\n", sum, retv);
	return retv;
}
//fft 接收电平
double spi_fpga_rcv_fft1024_power(uint32_t *buf)
{
	int ret;
	
	ret = fpga_read_scope( 328, buf);
	if(ret < 0)
	{
		return -1;
	}
	else{
		return spi_fft(buf);
	}
}

//通信带宽计算 带宽为3.5 7 14 滚降系数为1.1666666 其余滚降系数为1.1200684
static double tx_symrate(uint32_t R08, uint32_t R10)
{
	uint32_t symrate = (R08 & 0xff);
	uint32_t symadjust = ((R10>>8) & 0xffffff);
	double roll_off_factor;
	if(symadjust == 335543)
	{
		roll_off_factor = 1.1666666;
	}else{
		roll_off_factor = 1.1200684;
	}
	
	double SR = (200.0f * symrate / 256.0f) * (pow(2,24)/(symadjust+pow(2,24)));
	return SR * roll_off_factor;	
}

static double rx_symrate(uint32_t R08, uint32_t R10)
{
	uint32_t symrate = (R08 & 0xff);
	uint32_t symadjust = ((R10>>8) & 0xffffff);
	double roll_off_factor;
	if(symadjust == 349524)
	{
		roll_off_factor = 1.1666666;
	}else{
		roll_off_factor = 1.1200684;
	}
	
	double SR = (200.0f * symrate / 256.0f) * (pow(2,24)/(symadjust+pow(2,24)));
	return SR * roll_off_factor;	
}

//调制类型
char *s_mod_index_arr[] = {"QAM256", "QAM128", "QAM1024", "QAM64", "QAM512", "QAM32", "", "QAM16", "", "QAM8", "", "", "", "QPSK", "", "AUTO"};
//static char *r_mod_indx(uint32_t R08, uint32_t R04, uint32_t R0c)
static uint32_t r_mod_indx(uint32_t R08, uint32_t R04, uint32_t R0c)
{
	uint32_t mod_index;
	if(((R08 >> 8)&0x0f) == 15)
	{	
		mod_index = 15;
		//mod_index = ((R0c >> 0)&0xff);
	}else
	{
		mod_index = ((R08 >> 8)&0x0f);
	}
	//return s_mod_index_arr[mod_index];
	return mod_index;
}

static uint32_t recv_mod_indx()
{
	uint32_t buf[1024] = {0};
	uint32_t mod_index;
	
	fpga_read_scope( 328, buf);
	
	mod_index = ((buf[0] & 0xffff0000) >> 16) / 4096;

	return mod_index;
}

static uint32_t recv_mod_indx_pin()
{
	uint32_t buf[1024] = {0};
	uint32_t mod_index;
	
	wReg("evb_scope_sel", 0x0a);
	usleep(100);
	fpga_read_scope( 328, buf);
	
	mod_index = ((buf[0] & 0xffff0000) >> 16) / 4096;
	
	wReg("evb_scope_sel", 0x0);
	
	return mod_index;
}



//传输速率

//int s_mod2M_arr[] = {8, 7, 10, 6, 9, 5, 0, 4, 0, 3, 0, 0, 0, 2, 0, 1};
int s_mod2M_arr[] = {8, 7, 10, 6, 9, 5, 0, 4, 0, 3, 0, 0, 0, 2, 0, 1};
static uint32_t r_mod2M(uint32_t R08, uint32_t R04, uint32_t R70c)
{
	uint32_t mod_index;
	if(((R08 >> 8)&0x0f) == 15)
	{	
		mod_index = ((R70c >> 0)&0xff);
	}else
	{
		mod_index = ((R08 >> 8)&0x0f);
	}
	return s_mod2M_arr[mod_index];
}

static double r_THR(uint32_t R08, uint32_t R0c, uint32_t R10, uint32_t R04, uint32_t R70c)
{
	uint32_t symrate = (R08 & 0xff);
	uint32_t symadjust = ((R10>>8) & 0xffffff);
	uint32_t fec_cw_len = (R0c & 0xff);
	uint32_t fec_pl_len = ((R0c>>8) & 0xff);
	uint32_t wayside_len = ((R08>>24) & 0xff);
	uint32_t mod_index = r_mod2M(R08, R04, R70c);

	//printf("symrate=0x%x symadjust=0x%x fec_cw_len=0x%x fec_pl_len=0x%x wayside_len=0x%x mod_index=0x%x\n", symrate, symadjust, fec_cw_len, fec_pl_len, wayside_len, mod_index);
	double SR = (200.0f * symrate / 256.0f) * (pow(2,24)/(symadjust+pow(2,24)));
	double AFE = 16.0f*fec_cw_len/(23.0f+2*wayside_len+16.0f*fec_cw_len);
	double CR = (double)fec_pl_len/(double)fec_cw_len;
	double THR = SR * AFE * CR * (double) mod_index;
	//printf("SR=%f AFE=%f CR=%f THR=%f\n", SR, AFE, CR, THR);
	
	return THR;
}

//符号速率 Symbol_rate
static double Symbol_rate(uint32_t R08, uint32_t R10)
{
	uint32_t symrate = (R08 & 0xff);
	uint32_t symadjust = ((R10>>8) & 0xffffff);
	
	double SR = (200.0f * symrate / 256.0f) * (pow(2,24)/(symadjust+pow(2,24)));
	return SR;
}
//交织深度Interleavel
static uint32_t r_ilv_depth(uint32_t data)
{
	return ((data>>16)&0xff);
}

//编码速率 Code rate
static uint32_t r_fec_cw_len(uint32_t data)
{
	return ((data)&0xff);
}

static uint32_t r_fec_pl_len(uint32_t data)
{
	return ((data>>8)&0xff);
}

//杂散Distortion
static uint32_t r_Distortion_AMAM(uint32_t data)
{
	return (data&0xff);
}
static uint32_t r_Distortion_AMAP(uint32_t data)
{
	return ((data>>16)&0xff);
}

//数字自回环Digital Loopback 读取寄存器rx_digital_loop_ena
char *Loopback[] = { "Digital_Loopback=0", "Digital_Loopback=1"};
static char *Digital_LoopbackM(uint32_t data)
{
	uint32_t digital_Loopback= ((data)&0x01);
	return Loopback[digital_Loopback];
}

//增益值DA的数值
int get_DA()
{
	int wdata;
	char buf[128]={0};
	char Dagain[4]={0};
	
	FILE *fp;
	
	if((fp = fopen("config.txt","r+"))==NULL)//打开文件，之后判断是否打开成功
	{
		perror("cannot open file");
		exit(0);
	}
	while(fgets(buf,128,fp)!=NULL)
	{

		if(buf[0] == '5')
		{
			bzero(Dagain,3);
			Dagain[0] = buf[9];
			Dagain[1] = buf[10];
			Dagain[2] = buf[11];
			Dagain[3] = buf[12];			
			wdata = atoi( Dagain);
			printf("buf=%s wdata=%d\n", buf, wdata);
		}
	}
	fclose(fp);
	return wdata;
}
//level的值
int get_AD()
{
	int wdata;
	char buf[128]={0};
	char Adgain[4]={0};
	
	FILE *fp;
	
	if((fp = fopen("config.txt","r+"))==NULL)//打开文件，之后判断是否打开成功
	{
		perror("cannot open file");
		exit(0);
	}
	while(fgets(buf,128,fp)!=NULL)
	{

		if(buf[0] == '6')
		{
			bzero(Adgain,3);
			Adgain[0] = buf[9];
			Adgain[1] = buf[10];
			Adgain[2] = buf[11];
			Adgain[3] = buf[12];			
			wdata = atoi( Adgain);
			printf("buf=%s wdata=%d\n", buf, wdata);
		}
		bzero(buf,128);
	}
	fclose(fp);
	return wdata;
}

//微波高低栈的编号
int get_WB()
{
	int wdata;
	char buf[128]={0};
	char WB_mode[4]={0};
	
	FILE *fp;
	
	if((fp = fopen("config.txt","r+"))==NULL)//打开文件，之后判断是否打开成功
	{
		perror("cannot open file");
		exit(0);
	}
	while(fgets(buf,128,fp)!=NULL)
	{

		if(buf[0] == '7')
		{
			bzero(WB_mode,3);
			WB_mode[0] = buf[9];
			WB_mode[1] = buf[10];
			WB_mode[2] = buf[11];
			WB_mode[3] = buf[12];			
			wdata = atoi( WB_mode);
			printf("buf=%s wdata=%d\n", buf, wdata);
		}
		bzero(buf,128);
	}
	fclose(fp);
	return wdata;
}

//FPGA版本
int get_FPGA()
{
	int wdata;
	char buf[128]={0};
	char FPGA_mode[4]={0};
	
	FILE *fp;
	
	if((fp = fopen("config.txt","r+"))==NULL)//打开文件，之后判断是否打开成功
	{
		perror("cannot open file");
		exit(0);
	}
	while(fgets(buf,128,fp)!=NULL)
	{

		if(buf[0] == '2')
		{
			bzero(FPGA_mode,3);
			FPGA_mode[0] = buf[7];
			FPGA_mode[1] = buf[8];
			FPGA_mode[2] = buf[9];
			FPGA_mode[3] = buf[10];			
			wdata = atoi( FPGA_mode);
			printf("buf=%s wdata=%d\n", buf, wdata);
		}
		bzero(buf,128);
	}
	fclose(fp);
	return wdata;
}

//误码率
static uint32_t uncoded_ber(uint32_t data)
{
	return ((data>>16)&0xff);
}
static uint32_t coded_ber(uint32_t data)
{
	return ((data>>16)&0xff);
}
static uint32_t cumulative_be(uint32_t data)
{
	return ((data>>16)&0xff);
}

//获取版本号
static uint32_t r_ver_minor(uint32_t data)
{
	return (data&0x0f);
}


static uint32_t r_ver_major(uint32_t data)
{
	return ((data>>8)&0x0f);
}

//获取本地MAC IP 时间
int systemx(char *result, int size, const char *format, ...)
{
    int ret;
    char cmd[1024] = {0};
    
    va_list args;   
    va_start(args, format);
    ret = vsnprintf(cmd, 1024, format, args);
    va_end (args);
    
    //printf("system_p cmd: %s\n", cmd);
	if (ret < 0 || ret >= 1024){ printf("vsnprintf failed, cmd %s.\n", cmd); return -1;};
	
	FILE * fp;
	if ((fp = popen(cmd, "r")) == NULL)	return -1;
	if (result && size > 0)
	{ 
		int ret = fread(result, 1, size, fp);
		if (ret == size) ret -= 1; 
		*(result + ret) = '\0';
	};
	
	return pclose(fp);
}

int get_eth0_IP(char *buf, int buf_size)
{
	return systemx(buf, buf_size, "ifconfig eth0 | grep \"inet addr\" | awk '{ print $2}' | awk -F: '{print $2}' | xargs echo -n");
}

int get_eth0_MAC(char *buf, int buf_size)
{
	return systemx(buf, buf_size, "ifconfig eth0 | grep \"HWaddr\" | awk '{ print $5}' | xargs echo -n");
}

int get_uptime(char *buf, int buf_size)
{
	return systemx(buf, buf_size, "cat /proc/uptime | awk '{ print $1}'");
}

int get_uptime_str(char *buf, int len)
{
	char uptime_buf[32] = {0};
	get_uptime(uptime_buf, 30);

	int uptime_secs = atoi(uptime_buf);
	int nday = uptime_secs/(3600*24);
	uptime_secs %= (3600*24);
	int HH = uptime_secs/(3600);
	uptime_secs %= (3600);
	int MM = uptime_secs/(60);
	uptime_secs %= (60);
	int SS = uptime_secs;
	if (nday > 0)
		snprintf(buf, len, "%d天 %02d:%02d:%02d", nday, HH, MM, SS);  
	else
		snprintf(buf, len, "%02d:%02d:%02d", HH, MM, SS);  

	return 0;
}

int get_ip_mac(char *buf, int len)
{
	char IP[32] = {0};	
	char MAC[32] = {0};	
	get_eth0_IP(IP, 30);
	get_eth0_MAC(MAC, 30);

	snprintf(buf, len, "IP=%s, MAC=%s", IP, MAC);
	return 0;
}

int runinfo(char *buf, int len)
{
	char uptime_buf[32] = {0};
	get_uptime_str(uptime_buf, 30);

	snprintf(buf, len, "status=0, uptime=%s", uptime_buf);
	return 0;
}

int read_stonmsg(char *buf_name, char *buf_longitude, char *buf_atitude, int len)
{
	FILE *fpston;
	char stonbuf[256] = {0};	
	
	if((fpston = fopen("stationmsg.txt","r+"))==NULL)//打开文件，之后判断是否打开成功
	{
		perror("cannot open file");
		exit(0);
	}
	
	fgets( stonbuf, 256, fpston);
	strcpy(buf_name, stonbuf);
	fgets( stonbuf, 256, fpston);
	strcpy(buf_longitude, stonbuf);
	fgets( stonbuf, 256, fpston);
	strcpy(buf_atitude, stonbuf);
	

	fclose(fpston);
	return 0;
}

int get_date(char *date, int len)
{
	time_t timep;
	time (&timep);
	snprintf( date, len, "%s", ctime(&timep));
	date[strlen(date)-1] = '\0';
	return 0;
}

int get_temperature()  
{  
  
    int fdWB, res, i, count;  
    struct termios  oldtio, newtio;  
    char buf[256] = {0}; 
	int tem;
	char t[4];
//-----------打开uart设备文件------------------  
    fdWB = open(UART_DEVICE, O_RDWR|O_NOCTTY);
    if (fdWB < 0) {  
        perror(UART_DEVICE);  
        exit(-1);  
    }  
    else  
        printf("Open %s successfully\n", UART_DEVICE);  

//-----------设置操作参数-----------------------    
    tcgetattr(fdWB, &oldtio);//获取当前操作模式参数  
    memset( &newtio, 0, sizeof(newtio));  
	
    //波特率=115200 数据位=8 使能数据接收   
    newtio.c_cflag = B115200|CS8|CLOCAL|CREAD;  
    newtio.c_iflag = IGNPAR;   
    //newtio.c_oflag = OPOST | OLCUC; //  
    /* 设定为正规模式 */  
    //newtio.c_lflag = ICANON;  
  
    tcflush(fdWB, TCIFLUSH);//清空输入缓冲区和输出缓冲区  
    tcsetattr(fdWB, TCSANOW, &newtio);//设置新的操作参数  

//-------------从uart接收数据-------------------  	
	char data_buf[24];
	
	i = 0;
	count = 0;
	while(1)
	{
		res = read( fdWB, buf, 255);//程序将在这里挂起，直到从uart接收到数据（阻塞操作）  
		count++;
		if(count > 1000000)
		{
			printf("UART1 timeout!\n");
			return -1;
		}
		if (res == 0)   
			continue;
		data_buf[i++] = buf[0];
		if(i > 20)
		{
			printf("data_buf = %s\n", data_buf);
			break;
		}
	}
	
	for(i = 0; i < 24; i++)
	{
		if(data_buf[i] == 'p')
		{
			pin = data_buf[i + 1];
		}
		if(pin == '0' || pin == '1')
			break;
	}
	for(i = 0; i < 24; i++)
	{
		if(data_buf[i] == 'l')
		{
			lock = data_buf[i + 1];
		}
		if(lock == '0' || lock == '1')
			break;
	}
	for(i = 0; i < 24; i++)
	{
		if(data_buf[i] == 't')
		{
			t[0] = buf[i + 1];
			t[1] = buf[i + 2];
			t[2] = buf[i + 3];
			t[3] = buf[i + 4];
			t[4] = '\0';
		}		
		tem = atoi(t);
		if(tem > 0)
			break;
	}
	printf("p=%c l=%c tem=%d\n", pin, lock, tem);

//------------关闭uart设备文件，恢复原先参数--------  
    close(fdWB);  
    printf("Close %s\n", UART_DEVICE);  
    tcsetattr(fdWB, TCSANOW, &oldtio); //恢复原先的设置  
  
    return 1;  
}

int sht20_t()
{
	int fd;
	int slaveaddr;
	uint8_t readbuf[4];
	uint8_t sendbuf[2];
	float tem;
	int t;
	int sockfd = static_socket;
	
	struct i2c_rdwr_ioctl_data data;
	data.msgs = (struct i2c_msg *) malloc(sizeof(struct i2c_msg));
	
	//1.打开通用设备文件
	if((fd = open(I2C_DEV, O_RDWR)) < 0)
	{
		printf("i2c device open failed...\n");
        return (-1);
	}
	
	slaveaddr = 0x40;
	
	ioctl(fd,I2C_TENBIT,0);   //not 10bit
	ioctl(fd,I2C_SLAVE,slaveaddr);   //设置I2C从设备地址[6:0]
	
	memset(sendbuf,0,2);	
	sendbuf[0] = 0xe3;
	write(fd, sendbuf, 1);
	memset(readbuf,0,4);
	read(fd, readbuf, 2);
	
	tem = 175.72 * (((((int) readbuf[0]) << 8) + readbuf[1]) / 65536.0) - 46.85;
	
	t = (int)(tem * 100);
	//6.关闭设备
	close(fd);
	
	if(readbuf[0] == 0)
		return -1;	
	return t;
}

int sht20_rh()
{
	int fd;
	int slaveaddr;
	uint8_t readbuf[4];
	uint8_t sendbuf[2];
	float rh;
	int r;
	int sockfd = static_socket;
	
	struct i2c_rdwr_ioctl_data data;
	data.msgs = (struct i2c_msg *) malloc(sizeof(struct i2c_msg));
	
	//1.打开通用设备文件
	if((fd = open(I2C_DEV, O_RDWR)) < 0)
	{
		printf("i2c device open failed...\n");
        return (-1);
	}
	
	slaveaddr = 0x40;
	
	ioctl(fd,I2C_TENBIT,0);   //not 10bit
	ioctl(fd,I2C_SLAVE,slaveaddr);   //设置I2C从设备地址[6:0]
	
	memset(sendbuf,0,2);
	sendbuf[0] = 0xe5;	
	write(fd, sendbuf, 1);
	memset(readbuf,0,4);
	read(fd, readbuf, 2);
	
	rh = 125 * (((((int) readbuf[0]) << 8) + readbuf[1]) / 65536.0) - 6;
	
	r = (int)(rh * 100);
	//6.关闭设备
	close(fd);
	
	if(readbuf[0] == 0)
		return -1;
	
	return r;
}

int lsusb_wifi()
{
	char buf[2];
	int buf_size;

	buf_size = 2;
	memset( buf, 0, 2);
	systemx(buf, buf_size, "lsusb |grep \"RTL8188CUS\" |wc -l");
	printf("lsusb wifi return %s\n", buf);
	if(buf[0] == '1')
		return 1;
	return -1;
}

int config_error()
{
	FILE *fp;
	
	if((fp = fopen("config.txt","r+")) == NULL) 
		return -1;
	
	fclose(fp);
	return 1;
}

void pthread_spi(void *arg)
{
    int ret, i;
	char msg[MSGSIZE] = {0};
	char msg_AD[1024] = {0};
	char netbuf[128] = {0};
	char timebuf[128] = {0};
	char stonmsg[256] = {0};
	char datebuf[128] = {0};
	time_t rawtime;
	struct tm *info;
	
	int sockfd = *((int *)arg);
	
	uint32_t buf[1024] = {0};
	char TLevelbuf[1024*10] = {0};
	double fft1024_power;
	double tlevel_avg[5];
	double avg_sum;
	flag = 0;
	flag1 = 0;
	flag2 = 0;
	flag3 = 0;
	flag4 = 0;
	char buf_longitude[16];
	char buf_atitude[16];

    fdFpga = open(device, O_RDWR);
    if (fdFpga < 0)
        pabort("can't open device");
	printf("fdFpga = %d\n",fdFpga);
    /*
     * spi mode
     */
    ret = ioctl(fdFpga, SPI_IOC_WR_MODE, &mode);
    if (ret == -1)
        pabort("can't set spi mode");

    ret = ioctl(fdFpga, SPI_IOC_RD_MODE, &mode);
    if (ret == -1)
        pabort("can't get spi mode");

    /*
     * bits per word
     */
    ret = ioctl(fdFpga, SPI_IOC_WR_BITS_PER_WORD, &bits);
    if (ret == -1)
        pabort("can't set bits per word");

    ret = ioctl(fdFpga, SPI_IOC_RD_BITS_PER_WORD, &bits);
    if (ret == -1)
        pabort("can't get bits per word");

    /*
     * max speed hz
     */
    ret = ioctl(fdFpga, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
    if (ret == -1)
        pabort("can't set max speed hz");

    ret = ioctl(fdFpga, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
    if (ret == -1)
        pabort("can't get max speed hz");

    printf("spi mode: %d\n", mode);
    printf("bits per word: %d\n", bits);
    printf("max speed: %d Hz (%d KHz)\n", speed, speed/1000);
	
	sockfd = static_socket;
	
	//读取配置文件获得增益、LEVEL、高低栈编号
	da_data = get_DA();
	ad_data = get_AD();
	wb_data = get_WB();
	fpga_data = get_FPGA();
	while(1)
	{
		/* microwave异常处理 */
		if((ret = get_temperature()) < 0)
		{
			memset( &error_msg, 0, sizeof(struct Error_msg));
			strcpy(error_msg.type,"error");
			error_msg.time = time(NULL);
			error_msg.number = 'm';
			if((ret = send( static_socket, &error_msg, sizeof(struct Error_msg), 0)) <= 0)
			{
				close(fdFpga);
				return;
			}
			sleep(1);
			printf("microwave Error!\n");
		}
		else
		{
			printf("microwave is ok!\n");
			break;
		}
	}

	/* USB-WIFI异常处理 */
	if((ret = lsusb_wifi()) < 0)
	{
		memset( &error_msg, 0, sizeof(struct Error_msg));
		strcpy(error_msg.type,"error");
		error_msg.time = time(NULL);
		error_msg.number = 'w';
		if((ret = send( static_socket, &error_msg, sizeof(struct Error_msg), 0)) <= 0)
		{
			close(fdFpga);
			return;
		}
		printf("WIFI Error!\n");
	}
	else
	{
		printf("USB-WIFI is ok!\n");
	}
	
	/* 配置文件异常处理 */
	if((ret = config_error()) < 0)
	{
		memset( &error_msg, 0, sizeof(struct Error_msg));
		strcpy(error_msg.type,"error");
		error_msg.time = time(NULL);
		error_msg.number = 'c';
		if((ret = send( static_socket, &error_msg, sizeof(struct Error_msg), 0)) <= 0)
		{
			close(fdFpga);
			return;
		}
		printf("配置文件 Error!\n");
	}
	else
	{
		printf("配置文件 is ok!\n");
	}
	

	/* sht20异常处理 */
	if((ret = sht20_t()) < 0)
	{
		memset( &error_msg, 0, sizeof(struct Error_msg));
		strcpy(error_msg.type,"error");
		error_msg.time = time(NULL);
		error_msg.number = 'l';
		if((ret = send( static_socket, &error_msg, sizeof(struct Error_msg), 0)) <= 0)
		{
			close(fdFpga);
			return;
		}
		printf("SHT20 Error!\n");
	}
	else
	{
		printf("SHT20 is ok!\n");
	}
	
	while(1)
	{
		usleep(10000);
		if(flag)
		{
			printf("__FILE__=%s __FUNCTION__=%s __LINE__=%d __DATE__=%s __TIME__=%s\n", __FILE__, __FUNCTION__, __LINE__, __DATE__, __TIME__);		
			
			close(fdFpga);
			printf("pthread_spi close\n");
			return;
		}	
		
		if(flag1 || flag2 || flag3 || flag4)
		{
			sockfd = static_socket;
			//IP MAC TIME
			get_ip_mac(netbuf, 100);
			runinfo(timebuf, 100);
			//read_stonmsg(stonmsg, 256);
			get_date(datebuf, 100);
			
			//寄存器的设定
			uint32_t R0 = fpga_read_uint32(0);
			uint32_t R4008 = fpga_read_uint32(0x4008);
			uint32_t R400c = fpga_read_uint32(0x400c);
			uint32_t R4010 = fpga_read_uint32(0x4010);
			uint32_t R4108 = fpga_read_uint32(0x4108);
			uint32_t R410c = fpga_read_uint32(0x410c);
			uint32_t R4110 = fpga_read_uint32(0x4110);
			uint32_t R414c = fpga_read_uint32(0x414c);
			uint32_t R4150 = fpga_read_uint32(0x4150); 
			
			uint32_t R4138 = fpga_read_uint32(0x4138); 
			uint32_t R4148 = fpga_read_uint32(0x4148);
			uint32_t R413c = fpga_read_uint32(0x413c);

			uint32_t R7004 = fpga_read_uint32(0x7004);
			uint32_t R700c = fpga_read_uint32(0x700c);
			
			uint32_t R4164 = fpga_read_uint32(0x4164);
			
			//传输速率
			double sndTHR,rcvTHR;
			sndTHR = r_THR(R4008, R400c, R4010, R7004, R700c);
			rcvTHR = r_THR(R4108, R410c, R4110, R7004, R700c);
			
			memset(tlevel_avg,0,5);
			memset(msg_AD, 0, 1024);	
			if(flag1) //status
			{
				//读取电平速率
				avg_sum = 0;
				for(i = 0; i < 4; i++)
				{
					tlevel_avg[i] = tlevel_avg[i+1];
				}
				fft1024_power = spi_fpga_rcv_fft1024_power(buf);
				if(fft1024_power == -1)
				{
					memset( &error_msg, 0, sizeof(struct Error_msg));
					strcpy(error_msg.type,"error");
					error_msg.time = time(NULL);
					error_msg.number = 'f';
					if((ret = send( sockfd, &error_msg, sizeof(struct Error_msg), 0)) <= 0)
					{
						close(fdFpga);
						return;
					}
				}
				else{
					tlevel_avg[4] = fft1024_power;
					for(i = 0; i < 5; i++)
					{
						avg_sum += tlevel_avg[i];
					}
					fft1024_power = avg_sum/5;
					
					//memset(msg_AD, 0, 1024);
					//sprintf( msg_AD, "serverip:%s,%s,version=AM_v0308_01,%s,%s;snd:TLevel=0.0 dBm,NBWidth=%.1f MHz,MType=%s, Symbol_rate=%.2f Mbd, Data_rate=%.2f Mbp/s, Ilv_depth=%d, Code_rate=%d/%d;rev:TLevel=%.2f dBm,NBWidth=%.1f MHz,MType=%s, Symbol_rate=%.2f Mbd, NSpeed=%.2f Mbp/s, Ilv_depth=%d, Code_rate=%d/%d, Distor_AMAM=%d, Distor_AMAP=%d, %s, Data_source=%d, DA_data=%d, AD_data=%d, bert_uncoded=%d, bert_coded=%d %d, mse_avg=%d, WB_data=%d;", 
						//netbuf, timebuf, stonmsg, datebuf, tx_symrate(R4008, R4010), r_mod_indx( R4008, R7004, R700c), Symbol_rate( R4008, R4010), sndTHR, r_ilv_depth(R4008), r_fec_cw_len(R400c), r_fec_pl_len(R400c), fft1024_power, rx_symrate(R4108, R4110), r_mod_indx( R4108, R7004, R700c), Symbol_rate( R4108, R4110), rcvTHR, r_ilv_depth(R4108), r_fec_pl_len(R400c), r_fec_cw_len(R400c), r_Distortion_AMAM(R4138), r_Distortion_AMAP(R4138), Digital_LoopbackM(R4148), rReg("tx_dbg_bert_ena"), da_data, ad_data, R414c, R4150, R4164, R413c, wb_data);
					memset( &statu_msg, 0, sizeof(statu_msg) );					
					strcpy(statu_msg.type,"status");
					statu_msg.version = ARMVERSION;
					printf("version%d\n",statu_msg.version);
					get_eth0_IP(statu_msg.ip,15);
					printf("ip%s\n",statu_msg.ip);
					get_eth0_MAC(statu_msg.mac,18);
					printf("mac%s\n",statu_msg.mac);
					get_uptime_str(statu_msg.uptime,16);
					printf("uptime%s\n",statu_msg.uptime);
					//get_date(statu_msg.date,28);
					time( &rawtime );
					info = localtime( &rawtime );
					strftime(statu_msg.date, 20, "%Y-%m-%d %H:%M:%S", info);
					printf("date%s\n",statu_msg.date);
					read_stonmsg( statu_msg.name, buf_longitude, buf_atitude, 64);
					printf("name%s  sizeof(name) %d\n", statu_msg.name, sizeof(statu_msg.name));
					statu_msg.longitude = strtod( buf_longitude, NULL);
					statu_msg.atitude = strtod( buf_atitude, NULL);
					printf("longitude%lf sizeof(longitude) %d\n", statu_msg.longitude, sizeof(statu_msg.longitude));
					printf("atitude%lf\n",statu_msg.atitude);
					statu_msg.TLevel = fft1024_power;
					printf("fft1024_power%lfTLevel%f\n", fft1024_power, statu_msg.TLevel);
					statu_msg.rTHR = rcvTHR;
					statu_msg.sTHR = sndTHR;
					printf("statu_msg.rTHR%0.2lf  statu_msg.sTHR%0.2f\n", statu_msg.rTHR, statu_msg.sTHR);
					statu_msg.NBWidth =  ((uint16_t)(tx_symrate(R4008, R4010)/1) << 8) + (uint16_t)(rx_symrate(R4108, R4110)/1);
					printf("NBWidth%d\n",statu_msg.NBWidth);
					statu_msg.QAM = (uint8_t)((recv_mod_indx_pin() << 4) + r_mod_indx( R4108, R7004, R700c));
					printf("QAM%d\n",statu_msg.QAM);
					statu_msg.Ilv_depth = (uint8_t)((r_ilv_depth(R4008) << 4) + r_ilv_depth(R4108));
					printf("Ilv_depth%d\n",statu_msg.Ilv_depth);
					statu_msg.Code_rate = (uint32_t)((r_fec_pl_len(R400c) << 24) + (r_fec_cw_len(R400c) << 16) + (r_fec_pl_len(R400c) << 8) + r_fec_cw_len(R400c));
					printf("Code_rate%u\n",statu_msg.Code_rate);
					statu_msg.Disto = (uint32_t)(r_Distortion_AMAM(R4138) << 16) + r_Distortion_AMAP(R4138);
					printf("Disto%d\n",statu_msg.Disto);
					statu_msg.one_bit = (uint8_t)((R4148 & 0x01) << 2) + ((rReg("tx_dbg_bert_ena") & 0x01) << 1) + ((int)(pin - '0') & 0x01);
					printf("one_bit%d\n",statu_msg.one_bit);
					statu_msg.DA_data = (uint8_t)da_data;
					printf("DA_data%d\n",statu_msg.DA_data);
					statu_msg.WB_data = (uint8_t)((ad_data & 0xf) << 4) + (wb_data & 0xf);
					printf("WB_data%d\n",statu_msg.WB_data);
					statu_msg.FPGA_data = (uint8_t)fpga_data;
					printf("FPGA_data%d\n",statu_msg.FPGA_data);
					statu_msg.bert_uncoded = R414c;
					printf("bert_uncoded%d\n",statu_msg.bert_uncoded);
					statu_msg.bert_coded = R4150;
					printf("bert_coded%d\n",statu_msg.bert_coded);
					statu_msg.mse_avg = R413c;
					printf("mse_avg%d\n",statu_msg.mse_avg);
					statu_msg.t = (uint16_t)sht20_t();
					printf("t%d\n",statu_msg.t);
					statu_msg.rh = (uint16_t)sht20_rh();
					printf("rh%d\n",statu_msg.rh);
					
					printf("struct Status_msg sizeof %d\n", sizeof(struct Status_msg));
					
					if((ret = send( sockfd, &statu_msg, sizeof(struct Status_msg), 0)) <= 0)
					{
						close(fdFpga);
						return;
					}
					//printf("%s\n",msg_AD);
					printf("***send status msg_AD success!\n");
					usleep(1000);
				}
			}
			
			if(flag2) //网卡
			{
				// sleep(1);
				// memset(msg_AD, 0, 1024);
				// sprintf( msg_AD, "ethn_version=%d.%d, rx_buffer_size=%u, tx_buffer_size=%u, local_loopback_enable=%u, loop_data_enable_override=%u, xon_threshold=%u, xoff_threshold=%u, xoff_rerequeset_timer=%u, pause_quanta=%u, rx_packets_detected=%u, rx_packets_dropped=%u, tx_packets_detected=%u, tx_packets_dropped=%u, mac_version=%u.%u, jumbo_frame_enable=%u/%u, vlan_enbale=%u/%u, rx_flow_control_enable=%u, tx_flow_control_enable=%u, max_frame_enable=%u/%u, max_frame_size=%u/%u, rx_bytes=%u, rx_good_frames=%u, rx_crc_errors=%u, rx_length_errors=%u, rx_pause_frames=%u, tx_bytes=%u, tx_good_frames=%u, tx_urun_errors =%u, tx_pause_frames=%u;", 
					// rReg("tdm_ver_major"), rReg("tdm_ver_minor"), rReg("eth_rx_fifo_len"), rReg("eth_tx_fifo_len"), rReg("eth_eth_local_loopback"), rReg("eth_eth_lb_ena_override"), rReg("eth_xon_thres"), rReg("eth_xoff_thres"), rReg("eth_xoff_timeout"), rReg("eth_pause_quanta"), rReg("eth_rx_detected"), rReg("eth_rx_dropped"), rReg("eth_tx_detected"), rReg("eth_tx_dropped"), rReg("eth_ver_major"), rReg("eth_ver_minor"), rReg("tme_conf_rx_jumbo_enable"), rReg("tme_conf_tx_jumbo_enable"), rReg("tme_conf_rx_vlan_enable"), rReg("tme_conf_tx_vlan_enable"), rReg("tme_conf_rx_flow_cntrl_ena"), rReg("tme_conf_tx_flow_cntrl_ena"), rReg("tme_conf_rx_max_frame_ena"), rReg("tme_conf_tx_max_frame_ena"), rReg("tme_conf_rx_max_frame"), rReg("tme_conf_tx_max_frame"), rReg("tme_stat_rx_bytes"), rReg("tme_stat_rx_good_frames"), rReg("tme_stat_rx_crc_errors"), rReg("tme_stat_rx_len_errors"), rReg("tme_stat_rx_pause_frames"), rReg("tme_stat_tx_bytes"), rReg("tme_stat_tx_good_frames"), rReg("tme_stat_tx_urun_errors"), rReg("tme_stat_tx_pause_frames"));
					
				// printf("%s\n",msg_AD);
				memset( &net_msg, 0, sizeof(net_msg) );
				strcpy(net_msg.type,"network");
				net_msg.ethn_version1 = rReg("tdm_ver_major");
				net_msg.ethn_version2 = rReg("tdm_ver_minor");
				net_msg.rx_buffer_size = rReg("eth_rx_fifo_len");
				net_msg.tx_buffer_size = rReg("eth_tx_fifo_len");
				net_msg.local_loopback_enable = rReg("eth_eth_local_loopback");
				net_msg.loop_data_enable_override = rReg("eth_eth_lb_ena_override");
				net_msg.xon_threshold = rReg("eth_xon_thres");
				net_msg.xoff_threshold = rReg("eth_xoff_thres");
				net_msg.xoff_rerequeset_timer;rReg("eth_xoff_timeout");
				net_msg.pause_quanta = rReg("eth_pause_quanta");
				net_msg.rx_packets_detected = rReg("eth_rx_detected");
				net_msg.rx_packets_dropped = rReg("eth_rx_dropped");
				net_msg.tx_packets_detected = rReg("eth_tx_detected");
				net_msg.tx_packets_dropped = rReg("eth_tx_dropped");
				net_msg.mac_version1 = rReg("eth_ver_major");
				net_msg.mac_version2 = rReg("eth_ver_minor");
				net_msg.jumbo_frame_enable1 = rReg("tme_conf_rx_jumbo_enable");
				net_msg.jumbo_frame_enable2 = rReg("tme_conf_tx_jumbo_enable");
				net_msg.vlan_enbale1 = rReg("tme_conf_rx_vlan_enable");
				net_msg.vlan_enbale2 = rReg("tme_conf_tx_vlan_enable");
				net_msg.rx_flow_control_enable = rReg("tme_conf_rx_flow_cntrl_ena");
				net_msg.tx_flow_control_enable = rReg("tme_conf_tx_flow_cntrl_ena");
				net_msg.max_frame_enable1 = rReg("tme_conf_rx_max_frame_ena");
				net_msg.max_frame_enable2 = rReg("tme_conf_tx_max_frame_ena");
				net_msg.max_frame_size1 = rReg("tme_conf_rx_max_frame");
				net_msg.max_frame_size2 = rReg("tme_conf_tx_max_frame");
				net_msg.rx_bytes = rReg("tme_stat_rx_bytes");
				net_msg.rx_good_frames = rReg("tme_stat_rx_good_frames");
				net_msg.rx_crc_errors = rReg("tme_stat_rx_crc_errors");
				net_msg.rx_length_errors = rReg("tme_stat_rx_len_errors");
				net_msg.rx_pause_frames = rReg("tme_stat_rx_pause_frames");
				net_msg.tx_bytes = rReg("tme_stat_tx_bytes");
				net_msg.tx_good_frames = rReg("tme_stat_tx_good_frames");
				net_msg.tx_urun_errors = rReg("tme_stat_tx_urun_errors");
				net_msg.tx_pause_frames = rReg("tme_stat_tx_pause_frames");
								
				if((ret = send( sockfd, &net_msg, sizeof(net_msg), 0)) <= 0)
				{
					close(fdFpga);
					return;
				}				
				printf("***send network msg_AD success!\n");
				sleep(1);
				
			}
			
			
			if(flag3)
			{
				//AD频谱采样
				wReg("evb_scope_sel", 0x0);
				
				ret = fpga_read_scope( 328, buf);
				printf("AD频谱采样");
				if(ret < 0)
				{
					memset( &error_msg, 0, sizeof(struct Error_msg));
					strcpy(error_msg.type,"error");
					error_msg.time = time(NULL);
					error_msg.number = 'm';
					if((ret = send( sockfd, &error_msg, sizeof(struct Error_msg), 0)) <= 0)
					{
						close(fdFpga);
						return;
					}
				}
				else{
					memset( &buf_msg, 0, sizeof(buf_msg) );
					for(i = 0; i < 1024;i++)
					{
						buf_msg.buf[i] = buf[i];
					}					
					strcpy(buf_msg.type,"spect");
					buf_msg.version = ARMVERSION;					
					get_eth0_IP(buf_msg.ip,15);					
					get_eth0_MAC(buf_msg.mac,18);					
					get_uptime_str(buf_msg.uptime,16);					
					time( &rawtime );
					info = localtime( &rawtime );
					strftime(buf_msg.date, 20, "%Y-%m-%d %H:%M:%S", info);				
					read_stonmsg( buf_msg.name, buf_longitude, buf_atitude, 64);										
					buf_msg.longitude = strtod( buf_longitude, NULL);
					buf_msg.atitude = strtod( buf_atitude, NULL);					
					buf_msg.TLevel = fft1024_power;	
					buf_msg.rTHR = rcvTHR;
					buf_msg.sTHR = sndTHR;
					printf("statu_msg.rTHR%0.2lf  statu_msg.sTHR%0.2f\n", statu_msg.rTHR, statu_msg.sTHR);					
					buf_msg.NBWidth =  ((uint16_t)(tx_symrate(R4008, R4010)/1) << 8) + (uint16_t)(rx_symrate(R4108, R4110)/1);					
					buf_msg.QAM = (uint8_t)((r_mod_indx( R4008, R7004, R700c) << 4) + r_mod_indx( R4108, R7004, R700c));
					//buf_msg.QAM = (uint8_t)((recv_mod_indx_pin() << 4) + r_mod_indx( R4108, R7004, R700c));					
					buf_msg.Ilv_depth = (uint8_t)((r_ilv_depth(R4008) << 4) + r_ilv_depth(R4108));					
					buf_msg.Code_rate = (uint32_t)((r_fec_pl_len(R400c) << 24) + (r_fec_cw_len(R400c) << 16) + (r_fec_pl_len(R400c) << 8) + r_fec_cw_len(R400c));				
					buf_msg.Disto = (r_Distortion_AMAM(R4138) << 16) + r_Distortion_AMAP(R4138);					
					buf_msg.one_bit = ((R4148 & 0x01) << 2) + ((rReg("tx_dbg_bert_ena") & 0x01) << 1) + ((int)(pin - '0') & 0x01);					
					buf_msg.DA_data = da_data;					
					buf_msg.WB_data = ((ad_data & 0xf) << 4) + (wb_data & 0xf);	
					buf_msg.FPGA_data = (uint8_t)fpga_data;					
					buf_msg.bert_uncoded = R414c;					
					buf_msg.bert_coded = R4150;					
					buf_msg.mse_avg = R413c;					
					buf_msg.t = (uint16_t)sht20_t();
					buf_msg.rh = (uint16_t)sht20_rh();
					if((ret = send( sockfd, &buf_msg, sizeof(buf_msg), 0)) <= 0)
					{
						close(fdFpga);
						return;
					}
					printf("***send AD msg success!\n");
					usleep(1000);
				}
			}
			
			if(flag4)
			{
				//星座图采样
				wReg("evb_scope_sel", 0xa);
				
				ret = fpga_read_scope( 328, buf);
				printf("星座图采样");
				if(ret < 0)
				{	
					memset( &error_msg, 0, sizeof(struct Error_msg));
					strcpy(error_msg.type,"error");
					error_msg.time = time(NULL);
					error_msg.number = 'm';
					if((ret = send( sockfd, &error_msg, sizeof(struct Error_msg), 0)) <= 0)
					{
						close(fdFpga);
						return;
					}
				}
				else{
					// memset(msg, 0, MSGSIZE);
					// sprintf( msg, "%s;serverip:%s,%s,version=AM_v0308_01;snd:TLevel=0.0 dBm,NBWidth=%.1f MHz,MType=%s, Symbol_rate=%.2f Mbd, Data_rate=%.2f Mbp/s, Ilv_depth=%d, Code_rate=%d/%d;rev:TLevel=%.2f dBm,NBWidth=%.1f MHz,MType=%s, Symbol_rate=%.2f Mbd, NSpeed=%.2f Mbp/s, Ilv_depth=%d, Code_rate=%d/%d, Distor_AMAM=%d, Distor_AMAP=%d, %s, Data_source=%d, DA_data=%d, AD_data=%d, bert_uncoded=%d, bert_coded=%d %d, mse_avg=%d, WB_data=%d;", 
						// TLevelbuf, netbuf, timebuf, tx_symrate(R4008, R4010), r_mod_indx( R4008, R7004, R700c), Symbol_rate( R4008, R4010), sndTHR, r_ilv_depth(R4008), r_fec_cw_len(R400c), r_fec_pl_len(R400c), fft1024_power, rx_symrate(R4108, R4110), r_mod_indx( R4108, R7004, R700c), Symbol_rate( R4108, R4110), rcvTHR, r_ilv_depth(R4108), r_fec_pl_len(R400c), r_fec_cw_len(R400c), r_Distortion_AMAM(R4138), r_Distortion_AMAP(R4138), Digital_LoopbackM(R4148), rReg("tx_dbg_bert_ena"), da_data, ad_data, R414c, R4150, R4164, R413c, wb_data);
					memset( &buf_msg, 0, sizeof(buf_msg) );	
					for(i = 0; i < 1024;i++)
					{
						buf_msg.buf[i] = buf[i];
					}									
					strcpy(buf_msg.type,"constel");
					buf_msg.version = ARMVERSION;					
					get_eth0_IP(buf_msg.ip,15);					
					get_eth0_MAC(buf_msg.mac,18);					
					get_uptime_str(buf_msg.uptime,16);					
					time( &rawtime );
					info = localtime( &rawtime );
					strftime(buf_msg.date, 20, "%Y-%m-%d %H:%M:%S", info);		
					read_stonmsg( buf_msg.name, buf_longitude, buf_atitude, 64);										
					buf_msg.longitude = strtod( buf_longitude, NULL);
					buf_msg.atitude = strtod( buf_atitude, NULL);					
					buf_msg.TLevel = fft1024_power;	
					buf_msg.rTHR = rcvTHR;
					buf_msg.sTHR = sndTHR;
					printf("buf_msg.rTHR%0.2lf  buf_msg.sTHR%0.2f\n", statu_msg.rTHR, statu_msg.sTHR);					
					buf_msg.NBWidth =  ((uint16_t)(tx_symrate(R4008, R4010)/1) << 8) + (uint16_t)(rx_symrate(R4108, R4110)/1);					
					buf_msg.QAM = (uint8_t)((r_mod_indx( R4008, R7004, R700c) << 4) + r_mod_indx( R4108, R7004, R700c));
					//buf_msg.QAM = (uint8_t)((recv_mod_indx() << 4) + r_mod_indx( R4108, R7004, R700c));					
					buf_msg.Ilv_depth = (uint8_t)((r_ilv_depth(R4008) << 4) + r_ilv_depth(R4108));					
					buf_msg.Code_rate = (uint32_t)((r_fec_pl_len(R400c) << 24) + (r_fec_cw_len(R400c) << 16) + (r_fec_pl_len(R400c) << 8) + r_fec_cw_len(R400c));				
					buf_msg.Disto = (r_Distortion_AMAM(R4138) << 16) + r_Distortion_AMAP(R4138);					
					buf_msg.one_bit = ((R4148 & 0x01) << 2) + ((rReg("tx_dbg_bert_ena") & 0x01) << 1) + ((int)(pin - '0') & 0x01);					
					buf_msg.DA_data = da_data;					
					buf_msg.WB_data = ((ad_data & 0xf) << 4) + (wb_data & 0xf);
					buf_msg.FPGA_data = (uint8_t)fpga_data;							
					buf_msg.bert_uncoded = R414c;					
					buf_msg.bert_coded = R4150;					
					buf_msg.mse_avg = R413c;				
					buf_msg.t = (uint16_t)sht20_t();
					buf_msg.rh = (uint16_t)sht20_rh();
					if((ret = send( sockfd, &buf_msg, sizeof(buf_msg), 0)) <= 0)
					{
						close(fdFpga);
						return;
					}
					printf("***send Constellation msg success!\n");
					usleep(1000);
				}
			}
		}
	}
    close(fdFpga);
	return ;	
}
